﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(menuName = "Mafias's Crime World/Story", order = 999)]
public class ScriptableStory : ScriptableObject
{
    public int storyNo = -1;

    public Sprite image;
    public string _name;
    public string description;
    public int healthRequirement;

    public int attack;
    public int defense;

    public float strength;
    public float intelligence;

    public int moneyMin;
    public int moneyMax;

    public int crimePointMin;
    public int crimePointMax;

    static Dictionary<int, ScriptableStory> cache;
    public static Dictionary<int, ScriptableStory> dict
    {
        get
        {
            // not loaded yet?
            if (cache == null)
            {
                // get all ScriptableSkills in resources
                ScriptableStory[] stories = Resources.LoadAll<ScriptableStory>("");

                // check for duplicates, then add to cache
                List<string> duplicates = stories.ToList().FindDuplicates(story => story.name);
                if (duplicates.Count == 0)
                {
                    cache = stories.ToDictionary(enemy => enemy.name.GetStableHashCode(), story => story);
                }
                else
                {
                    foreach (string duplicate in duplicates)
                        Debug.LogError("Resources folder contains multiple ScriptableStory with the name " + duplicate + ".");
                }
            }
            return cache;
        }
    }
}
