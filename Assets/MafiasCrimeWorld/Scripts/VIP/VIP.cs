﻿// Buffs are like Skills, but for the Buffs list.
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

[Serializable]
public struct VIP
{
    public int hash;

    public DateTime vipTimeEnd;

    // constructors
    public VIP(ScriptableVIP data)
    {
        hash = data.name.GetStableHashCode();
        vipTimeEnd = GameManager.instance.ServerTime.Add(new TimeSpan(data.vipMonth * 30 + data.vipDay, data.vipHour, data.vipMinute, 0, 0));
    }
    public VIP(ScriptableVIP data, DateTime dt)
    {
        hash = data.name.GetStableHashCode();
        vipTimeEnd = dt;
    }

    [JsonIgnore]
    // wrappers for easier access
    public ScriptableVIP data
    {
        get
        {
            // show a useful error message if the key can't be found
            // note: ScriptableSkill.OnValidate 'is in resource folder' check
            //       causes Unity SendMessage warnings and false positives.
            //       this solution is a lot better.
            if (!ScriptableVIP.dict.ContainsKey(hash))
                throw new KeyNotFoundException("There is no ScriptableVIP with hash=" + hash + ". Make sure that all ScriptableVIP are in the Resources folder so they are loaded properly.");
            return (ScriptableVIP)ScriptableVIP.dict[hash];
        }
    }

    [JsonIgnore]
    public string name => data.name;
    [JsonIgnore]
    public Sprite image => data.image;

    public float VIPTimeRemaining()
    {
        // how much time remaining until the vip ends? (using server time)
        return (vipTimeEnd - GameManager.instance.ServerTime).TotalSeconds <= 0 ? 0 : (float)((vipTimeEnd - GameManager.instance.ServerTime).TotalSeconds);
    }
}