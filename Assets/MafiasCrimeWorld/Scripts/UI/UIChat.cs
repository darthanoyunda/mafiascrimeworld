﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIChat : MonoBehaviour {

    public static UIChat instance;

    public UIChatSlot slot;
    public Transform content;

    public InputField messageText;

    public string msgType = "local"; //local, whisper, guild

    public List<ChatMessage> localMessages = new List<ChatMessage>();
    public List<ChatMessage> whispers = new List<ChatMessage>();
    public List<ChatMessage> guildMessages = new List<ChatMessage>();

    private void Awake()
    {
        instance = this;
    }

    public void SetChattype(string _msgType)
    {
        msgType = _msgType;
    }

    // Update is called once per frame
    void Update()
    {
        /*if (Player.instance == null)
            return;

        Player player = Player.instance;*/

        switch (msgType)
        {
            case "local":
                {
                    UIUtils.BalancePrefabs(slot.gameObject, localMessages.Count, content);

                    for (int i = 0; i < localMessages.Count; ++i)
                    {
                        UIChatSlot slot = content.GetChild(localMessages.Count - 1 - i).GetComponent<UIChatSlot>();
                        slot.chatMessage = localMessages[i];
                    }
                }
                break;

            case "whisper":
                {
                    UIUtils.BalancePrefabs(slot.gameObject, whispers.Count, content);

                    for (int i = 0; i < whispers.Count; ++i)
                    {
                        UIChatSlot slot = content.GetChild(whispers.Count - 1 - i).GetComponent<UIChatSlot>();
                        slot.chatMessage = whispers[i];
                    }
                }
                break;

            case "guild":
                {
                    UIUtils.BalancePrefabs(slot.gameObject, guildMessages.Count, content);

                    for (int i = 0; i < guildMessages.Count; ++i)
                    {
                        UIChatSlot slot = content.GetChild(guildMessages.Count - 1 - i).GetComponent<UIChatSlot>();
                        slot.chatMessage = guildMessages[i];
                    }
                }
                break;
            default:
                break;
        }       
    }

    public void AddMessage(ChatMessage cm)
    {
        switch (cm.identifier)
        {
            case "/w":
                {
                    whispers.Add(cm);
                }
                break;
            case "/g":
                {
                    guildMessages.Add(cm);
                }
                break;
            case "":
                {
                    localMessages.Add(cm);
                }
                break;

            default:
                break;
        }
    }

    public void SendChatMessage()
    {
        Player.instance.SendChatMessage("", messageText.text, "");
        messageText.text = "";
    }
}
