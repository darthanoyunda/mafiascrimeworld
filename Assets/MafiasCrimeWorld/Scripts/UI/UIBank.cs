﻿using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIBank : MonoBehaviour {

    public InputField putMoneyIF;
    public InputField getMoneyIF;

    public Text moneyBankText;

    private void Awake()
    {
       
    }

    public void Update()
    {
        if (Player.instance == null)
            return;

        moneyBankText.text = Player.instance.moneyBank.ToString();        
    }

    public void PutMoney()
    {
        MafiasCrimeWorldClient.instance.PutMoneyBank(false, Convert.ToInt32(putMoneyIF.text));
        putMoneyIF.text = "";
    }
    public void GetMoney()
    {
        MafiasCrimeWorldClient.instance.PutMoneyBank(true, Convert.ToInt32(getMoneyIF.text));
        getMoneyIF.text = "";
    }
}
