﻿using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITrainShootingRange : MonoBehaviour {

    public string trainType = "";

    public Text healthText;
    public Text moneyText;
    public Text gainText;

    private void Awake()
    {
       
    }

    public void Update()
    {
        if (Player.instance == null)
            return;

        int outcome = 0;
        int gain = 0;
        int healthRequirement = 0;

        if (trainType == "STR")
        {
            gain = GameManager.instance.trainGainRates[Mathf.Min((int)Mathf.Floor(Player.instance.strength / 100f), 5)];
            outcome = GameManager.instance.trainMoneyRates[Mathf.Min((int)Mathf.Floor(Player.instance.strength / 100f), 5)];
            healthRequirement = GameManager.instance.trainHealthRates[Mathf.Min((int)Mathf.Floor(Player.instance.strength / 100f), 5)];

            healthText.text = healthRequirement.ToString();
            moneyText.text = outcome.ToString();
            gainText.text = gain.ToString();
        }
        else if (trainType == "INT")
        {
            gain = GameManager.instance.trainGainRates[Mathf.Min((int)Mathf.Floor(Player.instance.intelligence / 100f), 5)];
            outcome = GameManager.instance.trainMoneyRates[Mathf.Min((int)Mathf.Floor(Player.instance.intelligence / 100f), 5)];
            healthRequirement = GameManager.instance.trainHealthRates[Mathf.Min((int)Mathf.Floor(Player.instance.intelligence / 100f), 5)];

            healthText.text = healthRequirement.ToString();
            moneyText.text = outcome.ToString();
            gainText.text = gain.ToString();
        }
    }

    public void Train()
    {
        if (Player.instance != null && MafiasCrimeWorldClient.instance != null)
        {
            MafiasCrimeWorldClient.instance.TrainRequest(trainType);
        }
    }
}
