﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIArena : MonoBehaviour {

    public static UIArena instance;

    public UIArenaSlot slot;
    public Transform content;

    public Text arenaAttackTimer;

    private void Awake()
    {
        instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        if (Player.instance == null)
            return;

        Player player = Player.instance;
        

        float timeRemaining = (float)(player.lastArenaAttackTime - GameManager.instance.ServerTime).TotalSeconds;

        if (timeRemaining > 0)
        {
            TimeSpan ts = TimeSpan.FromSeconds(timeRemaining);

            arenaAttackTimer.text = ts.Hours + ":" + ts.Minutes + ":" + ts.Seconds;
            arenaAttackTimer.gameObject.SetActive(true);
        }
        else
        {
            arenaAttackTimer.gameObject.SetActive(false);
        }

        List<ArenaListDatas> arenaPlayerList = player.arenaPlayerList;

        UIUtils.BalancePrefabs(slot.gameObject, arenaPlayerList.Count, content);

        for (int i = 0; i < arenaPlayerList.Count; ++i)
        {
            UIArenaSlot slot = content.GetChild(i).GetComponent<UIArenaSlot>();
            slot.arenaData = arenaPlayerList[i];
        }
    }

}
