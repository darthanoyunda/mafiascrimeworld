﻿using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIPrison : MonoBehaviour {

    public Text jailFinishTime;
    public Text moneyText;

    public GameObject moneyToolbar;
    public Button giveABribeButton;
    public Button buttonTryEscape;
    
    public void Update()
    {
        if (Player.instance == null)
            return;


        jailFinishTime.gameObject.SetActive(Player.instance.bJailed);
        moneyToolbar.SetActive(Player.instance.bJailed);
        giveABribeButton.gameObject.SetActive(Player.instance.bJailed);
        buttonTryEscape.gameObject.SetActive(Player.instance.bJailed);

        if (Player.instance.bJailed)
        {
            float timeRemaining = (Player.instance.jailFinishTime - GameManager.instance.ServerTime).TotalSeconds <= 0 ? 0 : (float)(Player.instance.jailFinishTime - GameManager.instance.ServerTime).TotalSeconds;

            TimeSpan ts = TimeSpan.FromSeconds(timeRemaining);

            jailFinishTime.text = ts.Hours + ":" + ts.Minutes + ":" + ts.Seconds;
            
            moneyText.text = Player.instance.GetBribeMoney().ToString();
        }
    }

    public void GiveABribe()
    {
        if (Player.instance != null && MafiasCrimeWorldClient.instance != null)
        {
            MafiasCrimeWorldClient.instance.GiveABribeRequest();
        }
    }
    public void TryEscape()
    {
        if (Player.instance != null && MafiasCrimeWorldClient.instance != null && AdvertisementService.instance != null)
        {
#if UNITY_EDITOR
            CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                                     LocalizationManager.GetTermTranslation("unexpectedPlatform"),
                                     "OK", () => { CGUIPopUp.instance.Hide(); });
#elif UNITY_ANDROID
            CGUIPopUp.instance.ShowQuestion(LocalizationManager.GetTermTranslation("Info"),
                                     LocalizationManager.GetTermTranslation("showRewardedAdPrison"),
                                     LocalizationManager.GetTermTranslation("YES"), () => { CGUIPopUp.instance.Hide(); AdvertisementService.instance.ShowRewardedAds(rewardedAdsTypes.prison); },
                                     LocalizationManager.GetTermTranslation("NO"), () => { CGUIPopUp.instance.Hide(); });
#elif UNITY_IOS
           CGUIPopUp.instance.ShowQuestion(LocalizationManager.GetTermTranslation("Info"),
                                     LocalizationManager.GetTermTranslation("showRewardedAdPrison"),
                                     LocalizationManager.GetTermTranslation("YES"), () => { CGUIPopUp.instance.Hide(); AdvertisementService.instance.ShowRewardedAds(rewardedAdsTypes.prison); },
                                     LocalizationManager.GetTermTranslation("NO"), () => { CGUIPopUp.instance.Hide(); });
#else
            CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                                     LocalizationManager.GetTermTranslation("unexpectedPlatform"),
                                     "OK", () => { CGUIPopUp.instance.Hide(); });
#endif
        }
    }
}
