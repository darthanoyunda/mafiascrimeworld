﻿using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIStory : MonoBehaviour {

    public ScriptableStory story;
    public Image image;

    public Text nameText;
    public Text descText;

    public Text healthText;
    public Text moneyText;
    public Text crimePointText;

    public Button doJobButton;


    private void Awake()
    {
        if(story != null)
        {
            image.sprite = story.image;

            healthText.text = story.healthRequirement.ToString();
            moneyText.text = story.moneyMin.ToString() + "-" + story.moneyMax.ToString();
            crimePointText.text = story.crimePointMin.ToString() + "-" + story.crimePointMax;
        }
    }

    public void Update()
    {
        if (Player.instance == null)
            return;

        if (story != null)
        {
            nameText.text = LocalizationManager.GetTermTranslation(story._name);
            descText.text = LocalizationManager.GetTermTranslation(story.description);
        }

        doJobButton.gameObject.SetActive(story.storyNo == Player.instance.lastSucceedStoryNo + 1 );
    }

    public void DoStory()
    {
        float winRate = Player.instance.StorySuccessChance(ScriptableStory.dict[story.name.GetStableHashCode()]);
        
        string winRateText = "";

        if (winRate >= 80)
        {
            winRateText = "tooEasy";
        }
        else if (winRate >= 60)
        {
            winRateText = "easy";
        }
        else if (winRate >= 50)
        {
            winRateText = "balanced";
        }
        else if (winRate >= 20)
        {
            winRateText = "hard";
        }
        else if (winRate >= 0)
        {
            winRateText = "tooHard";
        }

        CGUIPopUp.instance.ShowQuestion(LocalizationManager.GetTermTranslation("enemyInfo"),
                                    LocalizationManager.GetTermTranslation(winRateText),
                                    LocalizationManager.GetTermTranslation("ATTACK"), () =>
                                    {
                                        if (Player.instance != null && MafiasCrimeWorldClient.instance != null)
                                        {
                                            MafiasCrimeWorldClient.instance.DoStoryRequest(story.name.GetStableHashCode());
                                        }
                                    },
                                    LocalizationManager.GetTermTranslation("GiveUp"), () => CGUIPopUp.instance.Hide());
    }
}
