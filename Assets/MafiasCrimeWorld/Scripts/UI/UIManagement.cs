﻿using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManagement : MonoBehaviour {

    public ScriptableManagement management;

    public Image image;

    public Text nameText;
    public Text descText;
    public Text ownedText;

    public Text moneyText;

    public Text incomeTimeText;
    public Button getIncomeButton;


    private void Awake()
    {
        if(management != null)
        {
            image.sprite = management.image;
        }
    }

    public void Update()
    {
        if (Player.instance == null)
            return;

        if (management != null)
        {
            nameText.text = LocalizationManager.GetTermTranslation(management._name);
            descText.text = LocalizationManager.GetTermTranslation(management.description);
        }

        float vipBonus = Player.instance.vip.hash != 0 ? Player.instance.vip.data.incomeIncreaser : 0;

        moneyText.text = management.buyPrice + "/" + Mathf.FloorToInt(management.income * (1 + vipBonus / 100)).ToString();

        int managementIndex = Player.instance.GetManagementIndexByName(management.name);

        if(managementIndex != -1)
        {
            ownedText.text = LocalizationManager.GetTermTranslation("owned").Replace("{owned}", Player.instance.managements[managementIndex].count.ToString());

            if (Player.instance.managements[managementIndex].GetIncomeTimeRemaining() > 0)
            {
                float timeRemaining = Player.instance.managements[managementIndex].GetIncomeTimeRemaining();

                TimeSpan ts = TimeSpan.FromSeconds(timeRemaining);

                incomeTimeText.text = ts.Hours + ":" + ts.Minutes + ":" + ts.Seconds;

                getIncomeButton.gameObject.SetActive(false);
                incomeTimeText.gameObject.SetActive(true);
            }
            else
            {
                getIncomeButton.gameObject.SetActive(true);
                incomeTimeText.gameObject.SetActive(false);
            }

        }
        else
        {
            ownedText.text = LocalizationManager.GetTermTranslation("notOwned");
            getIncomeButton.gameObject.SetActive(false);
            incomeTimeText.gameObject.SetActive(false);
        }

    }

    public void BuyManagement()
    {
        if(Player.instance != null && MafiasCrimeWorldClient.instance != null)
        {
            MafiasCrimeWorldClient.instance.BuyManagementRequest(management.name.GetStableHashCode());
        }
    }
    public void GetManagementIncome()
    {
        if (Player.instance != null && MafiasCrimeWorldClient.instance != null)
        {
            MafiasCrimeWorldClient.instance.GetIncomeManagementRequest(management.name.GetStableHashCode());
        }
    }
}
