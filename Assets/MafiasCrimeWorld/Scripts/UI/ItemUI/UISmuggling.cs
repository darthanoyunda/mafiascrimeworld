﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UISmuggling : MonoBehaviour {

    public static UISmuggling instance;

    public UISmugglingSlot slot;
    public Transform content;

    private void Awake()
    {
        instance = this;
    }

    // Update is called once per frame
    void Update () {
        if (Player.instance == null)
            return;

        Player player = Player.instance;

        List<ScriptableItemAndPrice> smugglingItem = player.smugglingItems;

        UIUtils.BalancePrefabs(slot.gameObject, smugglingItem.Count, content);

        for (int i = 0; i < smugglingItem.Count; ++i)
        {
            UISmugglingSlot slot = content.GetChild(i).GetComponent<UISmugglingSlot>();
            slot.index = i;
            slot.scriptableItem = ScriptableItem.dict[smugglingItem[i].hash];
            slot.price = smugglingItem[i].price;
        }
    }
}
