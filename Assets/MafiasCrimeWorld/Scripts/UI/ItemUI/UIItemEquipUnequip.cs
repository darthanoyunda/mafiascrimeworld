﻿using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIItemEquipUnequip : MonoBehaviour {

    public static UIItemEquipUnequip instance;
    public bool bInventory;
    public int no;


    public Image image;
    public Text itemName;
    public Text decsription;

    public Text attack;
    public Text bagCapacity;
    public Text defense;

    public Text strength;
    public Text intelligence;

    public Text strengthReq;
    public Text intelligenceReq;

    public Text equipUnequipText;
    public Button equipUnequipButton;
    
    public GameObject attackToolbar;
    public GameObject bagCapacityToolbar;

    public GameObject panel;

    private void Awake()
    {
        instance = this;
    }

    public void SetItem(bool _bInventory, int _no)
    {
        bInventory = _bInventory;
        no = _no;
    }

    public void Click()
    {
        if (bInventory && Player.instance.inventory.Count > no && no != -1)
        {
            ScriptableItemAndAmount scriptableItemAndAmount = Player.instance.inventory[no];
            ScriptableItem item = scriptableItemAndAmount.amount > 0 ? scriptableItemAndAmount.item : null;

            if (item != null)
            {
                image.sprite = item.image;
                itemName.text = item.name;
                decsription.text = item.description;

                if (item is EquipmentItem)
                {
                    int eqNo = Player.instance.GetEquipableIndex(item);
                    if(eqNo != -1)
                    {
                        MafiasCrimeWorldClient.instance.SwapItemRequest(no, eqNo);
                    }
                }
                
            }
        }
        else if (!bInventory && Player.instance.equipments.Count > no && no != -1)
        {
            Item item = Player.instance.equipments[no];

            if (item.hash != 0)
            {
                image.sprite = item.image;
                itemName.text = item.name;
                decsription.text = item.data.description;

                if (item.data is EquipmentItem)
                {
                    int invNo = Player.instance.GetFirstEmptyInventorySlot();
                    if (invNo != -1)
                    {
                        MafiasCrimeWorldClient.instance.SwapItemRequest(invNo, no);
                    }
                }
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Player.instance == null)
            return;

        if (bInventory && Player.instance.inventory.Count > no && no != -1)
        {
            ScriptableItemAndAmount scriptableItemAndAmount = Player.instance.inventory[no];
            ScriptableItem item = scriptableItemAndAmount.amount > 0 ? scriptableItemAndAmount.item : null;

            if (item != null)
            {
                image.sprite = item.image;
                itemName.text = item.name;
                decsription.text = item.description;

                if (item is EquipmentItem)
                {
                    attack.text = (item as EquipmentItem).attackBonus.ToString();
                    bagCapacity.text = (item as EquipmentItem).inventorySizeBonus.ToString();
                    defense.text = (item as EquipmentItem).defenseBonus.ToString();

                    attackToolbar.gameObject.SetActive((item as EquipmentItem).category != PlayerEquipmentTypes.Bag);
                    bagCapacityToolbar.gameObject.SetActive((item as EquipmentItem).category == PlayerEquipmentTypes.Bag);

                    strength.text = (item as EquipmentItem).strengthBonus.ToString();
                    intelligence.text = (item as EquipmentItem).intelligenceBonus.ToString();

                    strengthReq.text = (item as EquipmentItem).strengthRequirement.ToString();
                    intelligenceReq.text = (item as EquipmentItem).intelligenceRequirement.ToString();

                    attack.gameObject.SetActive(true);
                    defense.gameObject.SetActive(true);

                    strength.gameObject.SetActive(true);
                    intelligence.gameObject.SetActive(true);

                    strengthReq.gameObject.SetActive(true);
                    intelligenceReq.gameObject.SetActive(true);

                    equipUnequipText.text = LocalizationManager.GetTermTranslation("EQUIP");

                    panel.SetActive(true);
                }
                else
                {
                    attack.gameObject.SetActive(false);
                    bagCapacity.gameObject.SetActive(false);
                    defense.gameObject.SetActive(false);

                    strength.gameObject.SetActive(false);
                    intelligence.gameObject.SetActive(false);

                    strengthReq.gameObject.SetActive(false);
                    intelligenceReq.gameObject.SetActive(false);

                    panel.SetActive(false);
                }

            }
            else
            {
                panel.SetActive(false);
            }
        }
        else if (!bInventory && Player.instance.equipments.Count > no && no != -1)
        {
            Item item = Player.instance.equipments[no];

            if (item.hash != 0)
            {
                image.sprite = item.image;
                itemName.text = item.name;
                decsription.text = item.data.description;

                if (item.data is EquipmentItem)
                {
                    attack.text = (item.data as EquipmentItem).attackBonus.ToString();
                    defense.text = (item.data as EquipmentItem).defenseBonus.ToString();

                    strength.text = (item.data as EquipmentItem).strengthBonus.ToString();
                    intelligence.text = (item.data as EquipmentItem).intelligenceBonus.ToString();

                    strengthReq.text = (item.data as EquipmentItem).strengthRequirement.ToString();
                    intelligenceReq.text = (item.data as EquipmentItem).intelligenceRequirement.ToString();

                    attack.gameObject.SetActive(true);
                    defense.gameObject.SetActive(true);

                    strength.gameObject.SetActive(true);
                    intelligence.gameObject.SetActive(true);

                    strengthReq.gameObject.SetActive(true);
                    intelligenceReq.gameObject.SetActive(true);

                    equipUnequipText.text = LocalizationManager.GetTermTranslation("UNEQUIP");

                    panel.SetActive(true);
                }
                else
                {
                    attack.gameObject.SetActive(false);
                    defense.gameObject.SetActive(false);

                    strength.gameObject.SetActive(false);
                    intelligence.gameObject.SetActive(false);

                    strengthReq.gameObject.SetActive(false);
                    intelligenceReq.gameObject.SetActive(false);

                    panel.SetActive(false);
                }
            }
            else
            {
                panel.SetActive(false);
            }
        }
        else
        {
            panel.SetActive(false);
        }
    }
}
