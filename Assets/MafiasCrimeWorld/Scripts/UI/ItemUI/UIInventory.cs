﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIInventory : MonoBehaviour {

    public UIInventorySlot slot;
    public Transform content;
    	
	// Update is called once per frame
	void Update () {
        if (Player.instance == null)
            return;

        Player player = Player.instance;

        UIUtils.BalancePrefabs(slot.gameObject, Player.instance.inventory.Count, content);

        for (int i = 0; i < Player.instance.inventory.Count; ++i)
        {
            UIInventorySlot slot = content.GetChild(i).GetComponent<UIInventorySlot>();
            slot.no = i;
        }
    }
}
