﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIArenaSlot : MonoBehaviour {

    public Text noText;
    public Text nameText;
    public Text crimepointText;

    public ArenaListDatas arenaData;

    public Button attackButton;

    // Update is called once per frame
    void Update()
    {
        if (arenaData.no != -1)
        {
            noText.text = (arenaData.no + 1).ToString();
            nameText.text = arenaData.name.ToString();
            crimepointText.text = arenaData.crimePoints.ToString();
        }

        
        attackButton.gameObject.SetActive(arenaData.name != Player.instance.charName);
    }

    public void Click()
    {
        MafiasCrimeWorldClient.instance.ArenaAttackRequest(arenaData.name);
    }
}
