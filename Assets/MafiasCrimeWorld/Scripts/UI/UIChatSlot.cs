﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIChatSlot : MonoBehaviour {

    public Text senderText;
    public Text messageText;

    public ChatMessage chatMessage;

    // Update is called once per frame
    void Update()
    {
        if(Player.instance == null)
            return;

        Color textColor = Player.instance.vip.hash != 0 ? Player.instance.vip.data.chatMessageColor : Color.black;

        senderText.text = chatMessage.sender;
        messageText.text = chatMessage.message;

        senderText.color = textColor;
        messageText.color = textColor;
    }

    public void Click()
    {
        //MafiasCrimeWorldClient.instance.ReportChatMessageRequest(Player.instance.charName, chatMessage);
    }
}
