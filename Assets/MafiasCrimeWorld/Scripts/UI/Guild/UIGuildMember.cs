﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIGuildMember : MonoBehaviour {

    public Text textGuildMemberName;
    public Text textGuildMemberRank;
    public Text textCrimePoints;

    public Image imageOnline;

    public Button buttonPromote;
    public Button buttonDemote;
    public Button buttonKick;
    public Button buttonLeave;
    public Button buttonTerminate;

    // Update is called once per frame
    void Update ()
    {
        if (Player.instance == null)
            return;

        Player player = Player.instance;

        buttonPromote.interactable = player.guild.CanPromote(player.charName, textGuildMemberName.text);
        buttonDemote.interactable = player.guild.CanDemote(player.charName, textGuildMemberName.text);
        buttonKick.interactable = player.guild.CanKick(player.charName, textGuildMemberName.text);
        buttonLeave.interactable = player.guild.CanLeave(player.charName);
        buttonTerminate.interactable = player.guild.CanTerminate(player.charName);        

        if (player.charName == textGuildMemberName.text)
        {
            buttonKick.gameObject.SetActive(false);
            buttonLeave.gameObject.SetActive(player.guild.CanLeave(player.charName));
            buttonTerminate.gameObject.SetActive(player.guild.CanTerminate(player.charName));
        }
        else
        {
            buttonTerminate.gameObject.SetActive(false);
            buttonLeave.gameObject.SetActive(false);
            buttonKick.gameObject.SetActive(true);
        }
    }

    public void Promote()
    {
        MafiasCrimeWorldClient.instance.PromoteMemberRequest(textGuildMemberName.text);
    }
    public void Demote()
    {
        MafiasCrimeWorldClient.instance.DemoteMemberRequest(textGuildMemberName.text);
    }
    public void Kick()
    {
        MafiasCrimeWorldClient.instance.KickFromGuildRequest(textGuildMemberName.text);
    }
    public void Leave()
    {
        MafiasCrimeWorldClient.instance.LeaveGuildRequest();
    }
    public void Terminate()
    {
        MafiasCrimeWorldClient.instance.TerminateGuildRequest();
    }
}
