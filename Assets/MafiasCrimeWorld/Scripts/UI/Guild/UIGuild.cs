﻿using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIGuild : MonoBehaviour {

    public GameObject guildPanel;
    public GameObject noGuildPanel;

    public InputField CreateGuildInputField;
    public InputField SearchGuildInputField;

    public UIGuildListSlot guildListSlot;
    public Transform guildListGrid;

    public UIInvitedGuildSlot invitedGuildSlot;
    public Transform invitedGuildGrid;

    public Text textGuildCreationPrice;

    public Text textGuildName;
    public Text textGuildNotice;
    public Text textGuildMoney;
    public Text textGuildCrimePoints;

    public UIGuildMember guildMemberSlot;
    public Transform guildMembersGrid;

    public InputField inputFieldNotice;
    public Button buttonChangeNotice;

    public GameObject memberPanel;
    public Button buttonMemberPanelOpen;

    public UIAppliedGuildMember appliedGuildMemberSlot;
    public Transform appliedMembersGrid;

    public InputField inputFieldInvitePlayer;

    public Image newMemberRequestImage;

    public Sprite onlineImage;
    public Sprite offlineImage;

    // Update is called once per frame
    void Update () {

        if (Player.instance == null)
            return;

        Player player = Player.instance;

        if(string.IsNullOrEmpty(player.guild.name))
        {
            UIUtils.BalancePrefabs(invitedGuildSlot.gameObject, player.invitedGuilds.Count, invitedGuildGrid);

            for (int i = 0; i < player.invitedGuilds.Count; ++i)
            {
                UIInvitedGuildSlot slot = invitedGuildGrid.GetChild(i).GetComponent<UIInvitedGuildSlot>();
                slot.textGuildName.text = player.invitedGuilds[i];
            }

            UIUtils.BalancePrefabs(guildListSlot.gameObject, player.guildListForApply.Count, guildListGrid);

            for (int i = 0; i < player.guildListForApply.Count; ++i)
            {
                UIGuildListSlot slot = guildListGrid.GetChild(i).GetComponent<UIGuildListSlot>();
                slot.textGuildName.text = player.guildListForApply[i];
            }

            textGuildCreationPrice.text = GameManager.instance.GuildCreationPrice.ToString();

            guildPanel.SetActive(false);
            noGuildPanel.SetActive(true);
        }
        else
        {
            textGuildName.text = player.guild.name;
            textGuildNotice.text = player.guild.notice;
            textGuildMoney.text = player.guild.money.ToString();
            textGuildCrimePoints.text = player.guild.crimePoints.ToString();

            UIUtils.BalancePrefabs(guildMemberSlot.gameObject, player.guild.members.Count, guildMembersGrid);

            for (int i = 0; i < player.guild.members.Count; ++i)
            {
                UIGuildMember slot = guildMembersGrid.GetChild(i).GetComponent<UIGuildMember>();
                slot.textGuildMemberName.text = player.guild.members[i].name;
                slot.textGuildMemberRank.text = player.guild.members[i].rank.ToString();
                slot.textCrimePoints.text = player.guild.members[i].crimePoints.ToString();
                slot.imageOnline.sprite = player.guild.members[i].online ? onlineImage : offlineImage;
            }

            UIUtils.BalancePrefabs(appliedGuildMemberSlot.gameObject, player.guild.appliedPlayers.Count, appliedMembersGrid);

            for (int i = 0; i < player.guild.appliedPlayers.Count; ++i)
            {
                UIAppliedGuildMember slot = appliedMembersGrid.GetChild(i).GetComponent<UIAppliedGuildMember>();
                slot.memberName.text = player.guild.appliedPlayers[i];
            }

            newMemberRequestImage.gameObject.SetActive(player.guild.appliedPlayers.Count > 0);

            buttonChangeNotice.gameObject.SetActive(player.guild.CanNotify(player.charName));
            buttonMemberPanelOpen.gameObject.SetActive(player.guild.CanNotify(player.charName));

            noGuildPanel.SetActive(false);
            guildPanel.SetActive(true);
        }
	}

    public void CreateGuild()
    {
        if (Player.instance == null)
            return;

        Player player = Player.instance;

        if(player.money < GameManager.instance.GuildCreationPrice)
        {
            CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                                     LocalizationManager.GetTermTranslation("Notenoughtmoney"),
                                     "OK", () => { CGUIPopUp.instance.Hide(); });
            return;
        }

        if(player.vip.hash == 0)
        {
            CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                                        LocalizationManager.GetTermTranslation("NeedtohaveVIP"),
                                        "OK", () => { CGUIPopUp.instance.Hide(); });
            return;
        }
        MafiasCrimeWorldClient.instance.CreateGuildRequest(CreateGuildInputField.text);
    }

    public void SearchGuild()
    {
        MafiasCrimeWorldClient.instance.GetGuildNamesForApplyRequest(SearchGuildInputField.text);
    }

    public void ChangeGuildNotice()
    {
        if (Player.instance == null)
            return;

        Player player = Player.instance;

        if(player.guild.CanNotify(player.charName))
        {
            textGuildNotice.gameObject.SetActive(false);
            inputFieldNotice.gameObject.SetActive(true);
        }
    }

    public void SubmitGuildNotice()
    {
        MafiasCrimeWorldClient.instance.SetGuildNoticeRequest(inputFieldNotice.text);
        inputFieldNotice.gameObject.SetActive(false);
        textGuildNotice.gameObject.SetActive(true);
    }

    public void OpenMemberPanel(bool open)
    {
        memberPanel.gameObject.SetActive(open);
    }

    public void InvitePlayer()
    {
        MafiasCrimeWorldClient.instance.InviteToGuildRequest(inputFieldInvitePlayer.text);
    }
}
