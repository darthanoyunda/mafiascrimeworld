﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIAppliedGuildMember : MonoBehaviour {

    public Text memberName;

    public void AcceptApplyRequest()
    {
        MafiasCrimeWorldClient.instance.AcceptGuildApplyRequest(memberName.text);
    }
    public void RejectApplyRequest()
    {
        MafiasCrimeWorldClient.instance.RejectGuildApplyRequest(memberName.text);
    }
}
