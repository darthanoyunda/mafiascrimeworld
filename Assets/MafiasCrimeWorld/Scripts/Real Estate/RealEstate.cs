﻿// Buffs are like Skills, but for the Buffs list.
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

[Serializable]
public partial struct RealEstate
{
    public int hash;

    public DateTime lastCollectTime;

    public int count;

    // constructors
    public RealEstate(ScriptableRealEstate data)
    {
        hash = data.name.GetStableHashCode();
        lastCollectTime = GameManager.instance.ServerTime;
        count = 0;
    }
    public RealEstate(ScriptableRealEstate data, DateTime dt)
    {
        hash = data.name.GetStableHashCode();
        lastCollectTime = dt;
        count = 0;
    }
    public RealEstate(ScriptableRealEstate data, DateTime dt, int _count)
    {
        hash = data.name.GetStableHashCode();
        lastCollectTime = dt;
        count = _count;
    }

    [JsonIgnore]
    // wrappers for easier access
    public ScriptableRealEstate data
    {
        get
        {
            // show a useful error message if the key can't be found
            // note: ScriptableSkill.OnValidate 'is in resource folder' check
            //       causes Unity SendMessage warnings and false positives.
            //       this solution is a lot better.
            if (!ScriptableRealEstate.dict.ContainsKey(hash))
                throw new KeyNotFoundException("There is no ScriptableRealEstate with hash=" + hash + ". Make sure that all ScriptableRealEstate are in the Resources folder so they are loaded properly.");
            return ScriptableRealEstate.dict[hash];
        }
    }

    [JsonIgnore]
    public string name => data.name;
    [JsonIgnore]
    public Sprite image => data.image;

    public float GetIncomeTimeRemaining()
    {
        if((GameManager.instance.ServerTime - lastCollectTime).TotalHours > 18)
        {
            return 0;
        }
        else
        {
            if (GameManager.instance.ServerTime.Hour / 6 != lastCollectTime.Hour / 6)
            {
                return 0;
            }
            else
            {
                int nextIncomeHour = (6 * (1 + (int)Math.Floor((double)GameManager.instance.ServerTime.Hour / 6)));

                DateTime nextIncomeTime = GameManager.instance.ServerTime.AddHours(nextIncomeHour - GameManager.instance.ServerTime.Hour)
                    .AddMinutes(-1 * GameManager.instance.ServerTime.Minute)
                    .AddSeconds(-1 * GameManager.instance.ServerTime.Second)
                    .AddMilliseconds(-1 * GameManager.instance.ServerTime.Millisecond);
                

                return (float)(nextIncomeTime - GameManager.instance.ServerTime).TotalSeconds;
            }
        }
    }
}