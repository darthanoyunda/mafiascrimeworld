﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(menuName = "Mafias's Crime World/Real Estate", order = 999)]
public class ScriptableRealEstate : ScriptableObject
{
    public Sprite image;

    public string _name;
    public string description;

    public int buyPrice;
    public int income;

    static Dictionary<int, ScriptableRealEstate> cache;
    public static Dictionary<int, ScriptableRealEstate> dict
    {
        get
        {
            // not loaded yet?
            if (cache == null)
            {
                // get all ScriptableSkills in resources
                ScriptableRealEstate[] realEstates = Resources.LoadAll<ScriptableRealEstate>("");

                // check for duplicates, then add to cache
                List<string> duplicates = realEstates.ToList().FindDuplicates(enemy => enemy.name);
                if (duplicates.Count == 0)
                {
                    cache = realEstates.ToDictionary(realEstate => realEstate.name.GetStableHashCode(), realEstate => realEstate);
                }
                else
                {
                    foreach (string duplicate in duplicates)
                        Debug.LogError("Resources folder contains multiple ScriptableRealEstate with the name " + duplicate + ".");
                }
            }
            return cache;
        }
    }
}
