﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[CreateAssetMenu(menuName = "Mafias's Crime World/Items/Base Item", order = 999)]
public partial class ScriptableItem : ScriptableObject
{
    public int hash;

    [JsonIgnore]
    public string description;

    [Header("Base Stats")]
    [JsonIgnore]
    public long buyPrice;
    [JsonIgnore]
    public long sellPrice;
    [JsonIgnore]
    public bool sellable;
    [JsonIgnore]
    public int maxStack;
    [JsonIgnore]
    public Sprite image;

    [JsonIgnore]
    static Dictionary<int, ScriptableItem> cache;
    public static Dictionary<int, ScriptableItem> dict
    {
        get
        {
            // not loaded yet?
            if (cache == null)
            {
                // get all ScriptableItems in resources
                ScriptableItem[] items = Resources.LoadAll<ScriptableItem>("");

                // check for duplicates, then add to cache
                List<string> duplicates = items.ToList().FindDuplicates(item => item.name);
                if (duplicates.Count == 0)
                {
                    cache = items.ToDictionary(item => item.name.GetStableHashCode(), item => item);
                }
                else
                {
                    foreach (string duplicate in duplicates)
                        Debug.LogError("Resources folder contains multiple ScriptableItems with the name " + duplicate + ".");
                }
            }
            return cache;
        }
    }

    // validation //////////////////////////////////////////////////////////////
    public virtual void OnValidate()
    {
        // make sure that the sell price <= buy price to avoid exploitation
        // (people should never buy an item for 1 dollars and sell it for 2 dollars)
        sellPrice = Math.Min(sellPrice, buyPrice);

        hash = name.GetStableHashCode();
        
        if(maxStack < 1)
        {
            maxStack = 1;
        }
    }
}

// ScriptableItem + Amount is useful for default items (e.g. spawn with 10 potions)
[Serializable]
public struct ScriptableItemAndAmount
{
    public ScriptableItem item;
    public int amount;

    public ScriptableItemAndAmount(ScriptableItem _item, int _amount)
    {
        item = _item;
        amount = _amount;
    }

    // helper functions to increase/decrease amount more easily
    // -> returns the amount that we were able to increase/decrease by
    public int DecreaseAmount(int reduceBy)
    {
        // as many as possible
        int limit = Mathf.Clamp(reduceBy, 0, amount);
        amount -= limit;
        return limit;
    }

    public int IncreaseAmount(int increaseBy)
    {
        // as many as possible
        int limit = Mathf.Clamp(increaseBy, 0, item.maxStack - amount);
        amount += limit;
        return limit;
    }
}