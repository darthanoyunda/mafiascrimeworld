﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[CreateAssetMenu(menuName = "Mafias's Crime World/Shop List", order = 999)]
public partial class ScriptableShopList : ScriptableObject
{
    public int hash;
    [JsonIgnore]
    public string description;

    [JsonIgnore]
    public PlayerEquipmentTypes eqType;

    [JsonIgnore]
    public List<ScriptableItem> items;

    [JsonIgnore]
    static Dictionary<int, ScriptableShopList> cache;
    public static Dictionary<int, ScriptableShopList> dict
    {
        get
        {
            // not loaded yet?
            if (cache == null)
            {
                // get all ScriptableItems in resources
                ScriptableShopList[] shopLists = Resources.LoadAll<ScriptableShopList>("");

                // check for duplicates, then add to cache
                List<string> duplicates = shopLists.ToList().FindDuplicates(shopList => shopList.name);
                if (duplicates.Count == 0)
                {
                    cache = shopLists.ToDictionary(shopList => shopList.name.GetStableHashCode(), shopList => shopList);
                }
                else
                {
                    foreach (string duplicate in duplicates)
                        Debug.LogError("Resources folder contains multiple ScriptableShopList with the name " + duplicate + ".");
                }
            }
            return cache;
        }
    }

    // validation //////////////////////////////////////////////////////////////
    void OnValidate()
    {
        hash = name.GetStableHashCode();
    }
}