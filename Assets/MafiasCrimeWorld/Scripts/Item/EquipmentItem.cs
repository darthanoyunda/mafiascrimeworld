﻿using Newtonsoft.Json;
using System.Text;
using UnityEngine;

[CreateAssetMenu(menuName = "Mafias's Crime World/Items/Equipment", order = 999)]
public class EquipmentItem : UsableItem
{
    [Header("Equipment")]
    [JsonIgnore]
    public PlayerEquipmentTypes category;

    [JsonIgnore]
    public int attackBonus;
    [JsonIgnore]
    public int defenseBonus;

    [JsonIgnore]
    public float strengthBonus;
    [JsonIgnore]
    public float intelligenceBonus;

    [JsonIgnore]
    public int inventorySizeBonus;

    // can we equip this item into this specific equipment slot?
    public bool CanEquip(Player player, int inventoryIndex, int equipmentIndex)
    {
        PlayerEquipmentTypes requiredCategory = player.equipmentInfo[equipmentIndex].requiredCategory;

        return base.CanUse(player, inventoryIndex) &&
               requiredCategory != PlayerEquipmentTypes.None &&
               category == requiredCategory;
    }
}
