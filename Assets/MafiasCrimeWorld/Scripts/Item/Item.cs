﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

[Serializable]
public partial struct Item
{
    public int hash;

    // constructors
    public Item(ScriptableItem data)
    {
        hash = data.name.GetStableHashCode();
    }

    [JsonIgnore]
    // wrappers for easier access
    public ScriptableItem data
    {
        get
        {
            // show a useful error message if the key can't be found
            // note: ScriptableItem.OnValidate 'is in resource folder' check
            //       causes Unity SendMessage warnings and false positives.
            //       this solution is a lot better.
            if (!ScriptableItem.dict.ContainsKey(hash))
                throw new KeyNotFoundException("There is no ScriptableItem with hash=" + hash + ". Make sure that all ScriptableItems are in the Resources folder so they are loaded properly.");
            return ScriptableItem.dict[hash];
        }
    }


    [JsonIgnore]
    public string name => data.name;

    [JsonIgnore]
    public long buyPrice => data.buyPrice;

    [JsonIgnore]
    public long sellPrice => data.sellPrice;

    [JsonIgnore]
    public bool sellable => data.sellable;

    [JsonIgnore]
    public Sprite image => data.image;
}
