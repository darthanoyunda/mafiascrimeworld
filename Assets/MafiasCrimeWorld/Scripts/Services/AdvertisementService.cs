﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;
using System;
using I2.Loc;

public class AdvertisementService : MonoBehaviour
{
    public static AdvertisementService instance;

   


    public void Awake()
    {
        instance = this;
    }

    void Start()
    {
#if UNITY_ANDROID
        string appId = "ca-app-pub-8927685324534711~3046643938";
#elif UNITY_IPHONE
                    string appId = "ca-app-pub-8927685324534711~8107398922";
#else
        string appId = "unexpected_platform";
#endif

        MobileAds.Initialize(appId);

        rewardBasedVideo = RewardBasedVideoAd.Instance;
        rewardBasedVideo.OnAdFailedToLoad += HandleRewardBasedVideoFailedToLoad;
        rewardBasedVideo.OnAdRewarded += HandleRewardBasedVideoRewarded;
        rewardBasedVideo.OnAdClosed += HandleRewardBasedVideoClosed;
        RequestRewardBasedVideo();

        RequestInterstitial();
    }

    #region Rewarded Ads

    rewardedAdsTypes rewardedAdsType = rewardedAdsTypes.prison;

    public void ShowRewardedAds(rewardedAdsTypes rewardedAdsType)
    {
        if (rewardBasedVideo.IsLoaded())
        {
            this.rewardedAdsType = rewardedAdsType;

            if(rewardedAdsType == rewardedAdsTypes.prison)
            {
                //TODO: GameAnalytics
            }

            if(rewardBasedVideo.IsLoaded())
            {
                MafiasCrimeWorldClient.instance.ChangeTimeoutTime(60);
                rewardBasedVideo.Show();
            }
        }
    }

    public RewardBasedVideoAd rewardBasedVideo;


    private void RequestRewardBasedVideo()
    {
#if UNITY_ANDROID
        string adUnitId = "ca-app-pub-8927685324534711/8730215456";
#elif UNITY_IPHONE
            string adUnitId = "ca-app-pub-8927685324534711/7417133787";
#else
            string adUnitId = "unexpected_platform";
#endif
        AdRequest request = new AdRequest.Builder().Build();
        rewardBasedVideo.LoadAd(request, adUnitId);
    }



    public void HandleRewardBasedVideoFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        RequestRewardBasedVideo();

        CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                                    LocalizationManager.GetTermTranslation("FailedToLoadAds"),
                                    "OK", () => { CGUIPopUp.instance.Hide(); });
    }
    public void HandleRewardBasedVideoClosed(object sender, EventArgs args)
    {
        MafiasCrimeWorldClient.instance.ChangeTimeoutTime(5);
        RequestRewardBasedVideo();
    }
    public void HandleRewardBasedVideoRewarded(object sender, Reward args)
    {
        MafiasCrimeWorldClient.instance.ChangeTimeoutTime(5);
        switch (rewardedAdsType)
        {
            case rewardedAdsTypes.prison:
                MafiasCrimeWorldClient.instance.EscapeFromPrisonRequest();
                if (GameAnalyticsService.instance != null)
                {
                    GameAnalyticsService.instance.AddProgressionEvent(GameAnalyticsSDK.GAProgressionStatus.Complete, "rewardedAdsPrison", 1);
                }
                break;
            case rewardedAdsTypes.freeEnergy:
                MafiasCrimeWorldClient.instance.GetFreeEnergyRequest();
                break;
            case rewardedAdsTypes.freeMoney:
                MafiasCrimeWorldClient.instance.GetFreeMoneyRequest();
                break;
            default:
                break;
        }
    }


    #endregion

    #region Interstitial

    public InterstitialAd interstitial;

    public Action<bool> callback;

    public void RequestInterstitial()
    {
#if UNITY_ANDROID
                        string adUnitId = "ca-app-pub-8927685324534711/4966794919";
#elif UNITY_IPHONE
                            string adUnitId = "ca-app-pub-8927685324534711/5549248841";
#else
        string adUnitId = "unexpected_platform";
#endif
        this.interstitial = new InterstitialAd(adUnitId);

        this.interstitial.OnAdLoaded += HandleOnAdLoaded;
        this.interstitial.OnAdFailedToLoad += HandleOnAdFailedToLoad;
        this.interstitial.OnAdClosed += HandleOnAdClosed;

        AdRequest request = new AdRequest.Builder().Build();
        this.interstitial.LoadAd(request);
    }
    public void ShowInterstitial(Action<bool> callback)
    {
        this.callback = callback;
        if (interstitial.IsLoaded())
        {
            interstitial.Show();
        }
        else
        {
            callback(false);
        }
    }
    public void HandleOnAdLoaded(object sender, EventArgs args)
    {

    }

    public void HandleOnAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        RequestInterstitial();
    }


    public void HandleOnAdClosed(object sender, EventArgs args)
    {
        callback(true);
        RequestInterstitial();
    }

    #endregion
}

public enum rewardedAdsTypes
{
    prison = 0,
    freeEnergy = 1,
    freeMoney = 2,
}

