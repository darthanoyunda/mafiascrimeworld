﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

// guild ranks sorted by value. higher equals more power.
public enum GuildRank : byte
{
    Member = 0,
    Vice = 1,
    Master = 2
}

[Serializable]
public struct GuildMember
{
    // basic info
    public string name;
    public int crimePoints;
    public bool online;
    public GuildRank rank;

    public GuildMember(string name, int crimePoints, bool online, GuildRank rank)
    {
        this.name = name;
        this.crimePoints = crimePoints;
        this.online = online;
        this.rank = rank;
    }
}

[Serializable]
public struct Guild
{
    // Guild.Empty for ease of use
    public static Guild Empty = new Guild();

    // properties
    public string name;
    public string notice;
    public int crimePoints;
    public int money;
    public List<GuildMember> members;
    [JsonIgnore]
    public List<Player> onlineMembers;

    public List<string> appliedPlayers;

    public string master => members != null ? members.Find(m => m.rank == GuildRank.Master).name : "";

    // if we create a guild then always with a name and a first member
    public Guild(Player creator, string name, List<string> appliedPlayers, int crimePoints = 0, int money = 0)
    {
        this.name = name;
        notice = "";
        GuildMember member = new GuildMember(creator.charName, creator.crimePoints, true, GuildRank.Master);
        members = new List<GuildMember>();
        members.Add(member);
        onlineMembers = new List<Player>();

        if (creator != null)
            onlineMembers.Add(creator);

        this.crimePoints = crimePoints;
        this.money = money;

        this.appliedPlayers = appliedPlayers;
    }

    // can 'requester' leave the guild?
    // => not in GuildSystem because it needs to be available on the client too
    public bool CanLeave(string requesterName)
    {
        return members != null &&
               members.FindIndex(m => m.name == requesterName && m.rank != GuildRank.Master) != -1;
    }

    // can 'requester' terminate the guild?
    // => not in GuildSystem because it needs to be available on the client too
    public bool CanTerminate(string requesterName)
    {
        // only 1 person left, which is requester, which is the master?
        return members != null &&
               members.Count == 1 &&
               members.FindIndex(m => m.name == requesterName && m.rank == GuildRank.Master) != -1;
    }

    // can 'requester' change the notice?
    // => not in GuildSystem because it needs to be available on the client too
    public bool CanNotify(string requesterName)
    {
        return members != null &&
               members.FindIndex(m => m.name == requesterName && m.rank >= GameManager.instance.GuildNotifyMinRank) != -1;
    }

    // can 'requester' kick 'target'?
    // => not in GuildSystem because it needs to be available on the client too
    public bool CanKick(string requesterName, string targetName)
    {
        if (members != null)
        {
            int requesterIndex = members.FindIndex(m => m.name == requesterName);
            int targetIndex = members.FindIndex(m => m.name == targetName);
            if (requesterIndex != -1 && targetIndex != -1)
            {
                GuildMember requester = members[requesterIndex];
                GuildMember target = members[targetIndex];
                return requester.rank >= GameManager.instance.GuildKickMinRank &&
                       requesterName != targetName &&
                       target.rank != GuildRank.Master &&
                       target.rank < requester.rank;
            }
        }
        return false;
    }

    // can 'requester' invite 'target'?
    // => not in GuildSystem because it needs to be available on the client too
    public bool CanInvite(string requesterName, string targetName)
    {
        return members != null &&
               members.Count < GameManager.instance.GuildCapacity &&
               requesterName != targetName &&
               members.FindIndex(m => m.name == requesterName && m.rank >= GameManager.instance.GuildInviteMinRank) != -1;
    }

    // note: requester can't promote target to same rank, only to lower rank
    // => not in GuildSystem because it needs to be available on the client too
    public bool CanPromote(string requesterName, string targetName)
    {
        if (members != null)
        {
            int requesterIndex = members.FindIndex(m => m.name == requesterName);
            int targetIndex = members.FindIndex(m => m.name == targetName);
            if (requesterIndex != -1 && targetIndex != -1)
            {
                GuildMember requester = members[requesterIndex];
                GuildMember target = members[targetIndex];
                return requester.rank >= GameManager.instance.GuildPromoteMinRank &&
                       requesterName != targetName &&
                       target.rank + 1 < requester.rank;
            }
        }
        return false;
    }

    // can 'requester' demote 'target'?
    // => not in GuildSystem because it needs to be available on the client too
    public bool CanDemote(string requesterName, string targetName)
    {
        if (members != null)
        {
            int requesterIndex = members.FindIndex(m => m.name == requesterName);
            int targetIndex = members.FindIndex(m => m.name == targetName);
            if (requesterIndex != -1 && targetIndex != -1)
            {
                GuildMember requester = members[requesterIndex];
                GuildMember target = members[targetIndex];
                return requester.rank >= GameManager.instance.GuildPromoteMinRank &&
                       requesterName != targetName &&
                       target.rank > GuildRank.Member;
            }
        }
        return false;
    }
}
