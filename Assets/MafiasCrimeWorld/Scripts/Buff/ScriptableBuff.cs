﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(menuName = "Mafias's Crime World/Buff", order = 999)]
public class ScriptableBuff : ScriptableObject
{
    public Sprite image;

    public string _name;
    public string description;

    public int buffMonth = 0;
    public int buffDay = 0;
    public int buffHour = 0;
    public int buffMinute = 0;

    public int attackBonus;
    public int defenseBonus;

    public float strengthBonus;
    public float intelligenceBonus;

    public int inventorySizeBonus;

    // (has corrected target already)
    public void Apply(Player player)
    {
        // add buff or replace if already in there
        player.AddOrRefreshBuff(new Buff(this));
    }

    static Dictionary<int, ScriptableBuff> cache;
    public static Dictionary<int, ScriptableBuff> dict
    {
        get
        {
            // not loaded yet?
            if (cache == null)
            {
                // get all ScriptableSkills in resources
                ScriptableBuff[] buffs = Resources.LoadAll<ScriptableBuff>("");

                // check for duplicates, then add to cache
                List<string> duplicates = buffs.ToList().FindDuplicates(skill => skill.name);
                if (duplicates.Count == 0)
                {
                    cache = buffs.ToDictionary(buff => buff.name.GetStableHashCode(), buff => buff);
                }
                else
                {
                    foreach (string duplicate in duplicates)
                        Debug.LogError("Resources folder contains multiple ScriptableBuff with the name " + duplicate + ".");
                }
            }
            return cache;
        }
    }
}
