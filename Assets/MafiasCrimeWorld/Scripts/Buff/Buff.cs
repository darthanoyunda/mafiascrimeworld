﻿// Buffs are like Skills, but for the Buffs list.
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

[Serializable]
public partial class Buff
{
    public int hash;

    public DateTime buffTimeEnd;

    // constructors
    public Buff(ScriptableBuff data)
    {
        hash = data.name.GetStableHashCode();
        buffTimeEnd = GameManager.instance.ServerTime.Add(new TimeSpan( data.buffMonth * 30 + data.buffDay, data.buffHour, data.buffMinute, 0, 0));
    }
    public Buff(ScriptableBuff data, DateTime dt)
    {
        hash = data.name.GetStableHashCode();
        buffTimeEnd = dt;
    }

    public Buff(ScriptableBuff data, TimeSpan ts)
    {
        hash = data.name.GetStableHashCode();
        buffTimeEnd = GameManager.instance.ServerTime + ts;
    }

    [JsonIgnore]
    // wrappers for easier access
    public ScriptableBuff data
    {
        get
        {
            // show a useful error message if the key can't be found
            // note: ScriptableSkill.OnValidate 'is in resource folder' check
            //       causes Unity SendMessage warnings and false positives.
            //       this solution is a lot better.
            if (!ScriptableBuff.dict.ContainsKey(hash))
                throw new KeyNotFoundException("There is no ScriptableBuff with hash=" + hash + ". Make sure that all ScriptableBuff are in the Resources folder so they are loaded properly.");
            return (ScriptableBuff)ScriptableBuff.dict[hash];
        }
    }

    [JsonIgnore]
    public string name => data.name;
    [JsonIgnore]
    public Sprite image => data.image;

    public float BuffTimeRemaining()
    {
        // how much time remaining until the buff ends? (using server time)
        return (buffTimeEnd - GameManager.instance.ServerTime).TotalSeconds <= 0 ? 0 : (float)((buffTimeEnd - GameManager.instance.ServerTime).TotalSeconds);
    }
}