﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Player : MonoBehaviour {

    public static Player instance;

    public int connectionID;

    public List<ArenaListDatas> arenaPlayerList = new List<ArenaListDatas>();
    public DateTime lastAttackTime = GameManager.instance.ServerTime - new TimeSpan(0,10,0);
    public DateTime lastArenaAttackTime = GameManager.instance.ServerTime - new TimeSpan(0, 10, 0);

    public DateTime lastStealMoneyTime = GameManager.instance.ServerTime - new TimeSpan(0, 10, 0);
    public List<string> stealMoneyPlayerList = new List<string>();

    public DateTime jailFinishTime;   

    public bool bJailed
    {
        get
        {
            return (jailFinishTime - GameManager.instance.ServerTime).TotalSeconds <= 0 ? false : true;
        }
    }

    public void SetJailFinishTime(DateTime _jailFinishTime)
    {
        jailFinishTime = _jailFinishTime;

        if (bJailed)
        {
            if (GameAnalyticsService.instance != null)
            {
                GameAnalyticsService.instance.AddProgressionEvent(GameAnalyticsSDK.GAProgressionStatus.Complete, "jailed", 1);
            }
        }
    }

    public DateTime lastEnergyCollected = GameManager.instance.ServerTime - new TimeSpan(3, 0, 0);

    public float GetEnergyTimeRemaining()
    {
        if ((GameManager.instance.ServerTime - lastEnergyCollected).TotalHours > 21)
        {
            return 0;
        }
        else
        {
            if (GameManager.instance.ServerTime.Hour / 3 != lastEnergyCollected.Hour / 3)
            {
                return 0;
            }
            else
            {
                int nextEnergyHour = (3 * (1 + (int)Math.Floor((double)GameManager.instance.ServerTime.Hour / 3)));

                DateTime nextEnergyTime = GameManager.instance.ServerTime.AddHours(nextEnergyHour - GameManager.instance.ServerTime.Hour).
                    AddMinutes(-1 * GameManager.instance.ServerTime.Minute).
                    AddSeconds(-1 * GameManager.instance.ServerTime.Second).
                    AddMilliseconds(-1 * GameManager.instance.ServerTime.Millisecond);


                return (float)(nextEnergyTime - GameManager.instance.ServerTime).TotalSeconds;
            }
        }
    }

    public VIP vip = new VIP();

    public Guild guild = new Guild();
    public List<string> invitedGuilds = new List<string>();
    public List<string> guildListForApply = new List<string>();    

    public int playerID;
    public bool online;

    public string charName;

    public float _strength;
    public float strength
    {
        get
        {
            float equipmentBonus = 0;
            foreach (Item item in equipments)
                equipmentBonus += item.hash == 0 ? 0 : ((EquipmentItem)item.data).strengthBonus;

            float vipBonus = vip.hash != 0 ? vip.data.strengthBonus : 0;

            // base + buffs + equipments
            return 1 + vipBonus + _strength + equipmentBonus;
        }
    }

    public float _intelligence;
    public float intelligence
    {
        get
        {
            float equipmentBonus = 0;
            foreach (Item item in equipments)
                equipmentBonus += item.hash == 0 ? 0 : ((EquipmentItem)item.data).intelligenceBonus;

            float vipBonus = vip.hash != 0 ? vip.data.intelligenceBonus : 0;

            // base + buffs + equipments
            return 1 + vipBonus + _intelligence + equipmentBonus;
        }
    }

    public PlayerGrade playerGrade;
    public int crimePoints;
    public int money;
    public int moneyBank;

    public int lastEarnedMoney;

    public int totalPvEKill;
    public int totalPvEDeath;

    public int totalArenaKill;
    public int totalArenaDeath;

    public int health = 100;
    public int curHealth = 95;

    public float healthRegenerationTime;

    public int lastSucceedStoryNo = -1;

       
    [Header("Equipment Info")]
    public EquipmentInfo[] equipmentInfo = {
        new EquipmentInfo{requiredCategory= PlayerEquipmentTypes.Weapon, defaultItem=new ScriptableItemAndAmount()},
        new EquipmentInfo{requiredCategory= PlayerEquipmentTypes.Head, defaultItem=new ScriptableItemAndAmount()},
        new EquipmentInfo{requiredCategory= PlayerEquipmentTypes.Chest, defaultItem=new ScriptableItemAndAmount()},
        new EquipmentInfo{requiredCategory= PlayerEquipmentTypes.Hands, defaultItem=new ScriptableItemAndAmount()},
        new EquipmentInfo{requiredCategory= PlayerEquipmentTypes.Legs, defaultItem=new ScriptableItemAndAmount()},
        new EquipmentInfo{requiredCategory= PlayerEquipmentTypes.Feet, defaultItem=new ScriptableItemAndAmount()},
        new EquipmentInfo{requiredCategory= PlayerEquipmentTypes.Bag, defaultItem=new ScriptableItemAndAmount()}
    };

    public List<Item> equipments = new List<Item>();

    public List<Buff> buffs = new List<Buff>();

    public List<ScriptableItemAndAmount> inventory = new List<ScriptableItemAndAmount>();


    public List<RealEstate> realEstates = new List<RealEstate>();
    public List<Management> managements = new List<Management>();

    public List<AnnouncementDatas> announcements = new List<AnnouncementDatas>();

    public DateTime nextChangeTimeSmugglintItemsPrices;
    public List<ScriptableItemAndPrice> smugglingItems = new List<ScriptableItemAndPrice>();

    public List<Mail> mails = new List<Mail>();

    public int inventorySize
    {
        get
        {
            int equipmentBonus = 0;
            foreach (Item item in equipments)
                equipmentBonus += item.hash == 0 ? 0 : ((EquipmentItem)item.data).inventorySizeBonus;

            int vipBonus = vip.hash != 0 ? vip.data.inventorySizeBonus : 0;

            // base + buffs + equipments
            return equipmentBonus + vipBonus + GameManager.instance.playerInventorySize;
        }
    }

    public int attack
    {
        get
        {
            int equipmentBonus = 0;
            foreach (Item item in equipments)
                equipmentBonus += item.hash == 0 ? 0 : ((EquipmentItem)item.data).attackBonus;

            int vipBonus = vip.hash != 0 ? vip.data.attackBonus : 0;

            // base + buffs + equipments
            return (equipmentBonus + 5) * (1 + vipBonus / 100);
        }
    }
    public int defense
    {
        get
        {
            int equipmentBonus = 0;
            foreach (Item item in equipments)
                equipmentBonus += item.hash == 0 ? 0 : ((EquipmentItem)item.data).defenseBonus;

            int vipBonus = vip.hash != 0 ? vip.data.defenseBonus : 0;

            // base + buffs + equipments
            return (equipmentBonus + 5) * (1 + vipBonus/100);
        }
    }

    private void Awake()
    {
        instance = this;

        healthRegenerationTime = GameManager.instance.regenerationMinute * 60;

        if (DateTime.UtcNow.Hour % 2 == 0)
        {
            nextChangeTimeSmugglintItemsPrices = DateTime.UtcNow.AddHours(2).AddMinutes(-1 * DateTime.UtcNow.Minute).AddSeconds(-1 * DateTime.UtcNow.Second).AddMilliseconds(-1 * DateTime.UtcNow.Millisecond);
        }
        else
        {
            nextChangeTimeSmugglintItemsPrices = DateTime.UtcNow.AddHours(1).AddMinutes(-1 * DateTime.UtcNow.Minute).AddSeconds(-1 * DateTime.UtcNow.Second).AddMilliseconds(-1 * DateTime.UtcNow.Millisecond);
        }

        for (int i = 0; i< inventorySize; i++)
        {
            inventory.Add(new ScriptableItemAndAmount());
        }

        for (int i = 0; i < equipmentInfo.Length; i++)
        {
            if(equipmentInfo[i].defaultItem.amount > 0)
            {
                equipments.Add(new Item(equipmentInfo[i].defaultItem.item));
            }
            else
            {
                equipments.Add(new Item());
            }
        }       
    }
    
    // Use this for initialization
    void Start ()
    {
        if (Server.instance != null)
        {
            InvokeRepeating("IncreaseHealth", GameManager.instance.regenerationMinute * 60, GameManager.instance.regenerationMinute * 60);
        }        
}
    

    // Update is called once per frame
    void Update ()
    {
        CleanupBuffs();

        if (vip.hash != 0)
        {
            if(vip.VIPTimeRemaining() <= 0)
            {
                vip = new VIP();
            }
        }

        if (healthRegenerationTime > 0)
        {
            healthRegenerationTime -= Time.deltaTime;
        }
        else
        {
            healthRegenerationTime = 0;
        }            
    }

    public void ChangeCrimePoints(int difference)
    {
        crimePoints += difference;

        if (Server.instance != null)
        {
            Server.instance.SaveCrimePointMoney(this);
            Server.instance.SetGuildCrimePoint(this);
        }

        if (UIManager.instance != null)
        {
            if (difference > 0)
            {
                UIManager.instance.AnimateCrimePoints();
            }
        }
    }
    public void SetCrimePoints(int crimePoints)
    {
        int crimePointsDifference = crimePoints - this.crimePoints;

        this.crimePoints = crimePoints;

        if (Server.instance != null)
        {
            Server.instance.SaveCrimePointMoney(this);
            Server.instance.SetGuildCrimePoint(this);
        }

        if (UIManager.instance != null)
        {
            if (crimePointsDifference > 0)
            {
                UIManager.instance.AnimateCrimePoints();
            }
        }
    }

    public void ChangeMoney(int difference)
    {
        money += difference;

        if (Server.instance != null)
        {
            Server.instance.SaveCrimePointMoney(this);
        }

        if (UIManager.instance != null)
        {
            if (difference > 0)
            {
                UIManager.instance.AnimateMoney();
            }
        }

        if (difference > 0)
        {
            lastEarnedMoney = difference;
        }
    }
    public void SetMoney(int money)
    {
        int moneyDifference = money - this.money;

        this.money = money;

        if (Server.instance != null)
        {
            Server.instance.SaveCrimePointMoney(this);
        }

        if (UIManager.instance != null)
        {
            if (moneyDifference > 0)
            {
                UIManager.instance.AnimateMoney();
            }
        }

        if (moneyDifference > 0)
        {
            lastEarnedMoney = moneyDifference;
        }
    }

    public void ChangeCrimePointsMoney(int crimePoints, int money)
    {
        this.crimePoints += crimePoints;
        this.money += money;

        if (Server.instance != null)
        {
            Server.instance.SaveCrimePointMoney(this);
            Server.instance.SetGuildCrimePoint(this);
        }
        
            if (UIManager.instance != null)
            {
                if (money > 0)
                {
                    UIManager.instance.AnimateMoney();
                }
                if (crimePoints > 0)
                {
                    UIManager.instance.AnimateCrimePoints();
                }
            }

        if (money > 0)
        {
            lastEarnedMoney = money;
        }
    }

    public void SetCrimePointsMoney(int crimePoints, int money)
    {
        int moneyDifference = money - this.money;
        int crimePointsDifference = crimePoints - this.crimePoints;

        this.crimePoints = crimePoints;
        this.money = money;

        if (Server.instance != null)
        {
            Server.instance.SaveCrimePointMoney(this);
            Server.instance.SetGuildCrimePoint(this);
        }

        if (UIManager.instance != null)
        {
            if (moneyDifference > 0)
            {
                UIManager.instance.AnimateMoney();
            }
            if (crimePointsDifference > 0)
            {
                UIManager.instance.AnimateCrimePoints();
            }
        }

        if (moneyDifference > 0)
        {
            lastEarnedMoney = moneyDifference;
        }
    }

    public void SetCurHealth(int _curHealth)
    {
        int tempCurHealth = curHealth;

        curHealth = _curHealth;

        if(tempCurHealth >= 20 && curHealth < 20)
        {
            if(GameAnalyticsService.instance != null)
            {
                GameAnalyticsService.instance.AddProgressionEvent(GameAnalyticsSDK.GAProgressionStatus.Complete, "Minus20Health", curHealth);
            }
        }
    }

    public void IncreaseHealth()
    {
        if (String.IsNullOrEmpty(charName))
            return;

        int tempCurHealth = curHealth;

        curHealth += GameManager.instance.regenerationHealth;
        curHealth = Mathf.Min(health, curHealth);

        if (tempCurHealth != curHealth)
        {
            Server.instance.SyncHealth(this);
        }
    }

    // helper function to find a buff index
    public int GetBuffIndexByName(string buffName)
    {
        // (avoid FindIndex to minimize allocations)
        for (int i = 0; i < buffs.Count; ++i)
            if (buffs[i].name == buffName)
                return i;
        return -1;
    }

    public void AddOrRefreshBuff(Buff buff)
    {
        // reset if already in buffs list, otherwise add
        int index = GetBuffIndexByName(buff.name);
        if (index != -1) buffs[index] = buff;
        else buffs.Add(buff);
    }

    // helper function to remove all buffs that ended
    void CleanupBuffs()
    {
        for (int i = 0; i < buffs.Count; ++i)
        {
            if (buffs[i].BuffTimeRemaining() == 0)
            {
                buffs.RemoveAt(i);
                --i;
            }
        }
    }
    
    public void RefreshInventorySize()
    {
        if(inventory.Count < inventorySize)
        {
            for(int i = inventory.Count; i< inventorySize; i++)
            {
                inventory.Add(new ScriptableItemAndAmount());
            }
        }
    }

    public void SwapInventoryInventory(int fromIndex, int toIndex)
    {
        // note: should never send a command with complex types!
        // validate: make sure that the slots actually exist in the inventory
        // and that they are not equal
        if (0 <= fromIndex && fromIndex < inventory.Count &&
            0 <= toIndex && toIndex < inventory.Count &&
            fromIndex != toIndex)
        {
            // swap them
            ScriptableItemAndAmount temp = inventory[fromIndex];
            inventory[fromIndex] = inventory[toIndex];
            inventory[toIndex] = temp;
        }
    }

    // swap inventory & equipment slots to equip/unequip. used in multiple places
    public void SwapInventoryEquip(int inventoryIndex, int equipmentIndex)
    {
        // validate: make sure that the slots actually exist in the inventory
        // and in the equipment
        if (0 <= inventoryIndex && inventoryIndex < inventory.Count &&
            0 <= equipmentIndex && equipmentIndex < equipments.Count)
        {
            // item slot has to be empty (unequip) or equipable
            ScriptableItemAndAmount slot = inventory[inventoryIndex];

            if (slot.amount == 0 ||
                slot.item is EquipmentItem &&
                ((EquipmentItem)slot.item).CanEquip(this, inventoryIndex, equipmentIndex))
            {
                int inventorySizeChange = (slot.amount <= 0 || slot.item.hash == 0 ? 0 : ((EquipmentItem)slot.item).inventorySizeBonus) -
                    (equipments[equipmentIndex].hash == 0 ? 0 : (equipments[equipmentIndex].data as EquipmentItem).inventorySizeBonus);

                bool empty = true;
                for(int i = inventorySize + inventorySizeChange; i < inventorySize; i++)
                {
                    if(inventory[i].amount > 0)
                    {
                        empty = false;
                    }
                }

                if (inventorySizeChange >= 0 || empty)
                {
                    ScriptableItemAndAmount temp = equipments[equipmentIndex].hash == 0 ? new ScriptableItemAndAmount() : new ScriptableItemAndAmount(equipments[equipmentIndex].data, 1);
                    equipments[equipmentIndex] = slot.amount <= 0 || slot.item.hash == 0 ? new Item() : new Item(slot.item);
                    inventory[inventoryIndex] = temp;

                    if(inventorySizeChange > 0)
                    {
                        for (int i = 0; i < inventorySizeChange; i++)
                        {
                            inventory.Add(new ScriptableItemAndAmount());
                        }
                    }
                    else
                    {
                        inventory.RemoveRange(inventory.Count + inventorySizeChange, inventorySizeChange * -1);
                    }

                    if (Server.instance != null)
                        Server.instance.SwapItemSucceed(this, inventoryIndex, equipmentIndex);
                }
                else
                {
                    if (Server.instance != null)
                        Server.instance.TrySwapItemFailed(this, "bagisusing");
                }
            }
            else
            {
                if (Server.instance != null)
                    Server.instance.SwapItemFailed(this, "cantequip");
            }
        }
        else
        {
            if (Server.instance != null)
                Server.instance.TrySwapItemFailed(this, "error");
        }
    }

    public int GetEquipableIndex(ScriptableItem item)
    {
        if(item is EquipmentItem && item is EquipmentItem)
        {
            return equipmentInfo.ToList().FindIndex(x => x.requiredCategory == (item as EquipmentItem).category);
        }

        return -1;
    }

    public int GetFirstEmptyInventorySlot()
    {
        for(int i = 0; i < inventory.Count; i++)
        {
            if (inventory[i].amount <= 0)
                return i;
        }

        return -1;
    }

    #region Shop

    public void BuyShopItem(PlayerEquipmentTypes eqType, int index, int amount = 1)
    {
        if (bJailed && Server.instance != null)
        {
            Server.instance.TryBuyShopItemFailed(this, "Jailed");
            return;
        }

        List<ScriptableItem> shopItems = new List<ScriptableItem>();

        string shopListName = eqType.ToString() + "ShopList";

        if (ScriptableShopList.dict.ContainsKey(shopListName.GetStableHashCode()))
        {
            shopItems = ScriptableShopList.dict[shopListName.GetStableHashCode()].items;
        }

        // validate: close enough, npc alive and valid index?
        // use collider point(s) to also work with big entities
        if (0 <= index && index < shopItems.Count)
        {
            // valid amount?
            Item shopItem = new Item(shopItems[index]);
            if (1 <= amount)
            {
                long price = shopItem.buyPrice * amount;

                // enough gold and enough space in inventory?
                if (money >= price && InventoryCanAdd(shopItem, amount))
                {
                    // pay for it, add to inventory
                    //money -= (int)price;

                    ChangeCrimePointsMoney(0, -1 * (int)price);

                    InventoryAddItem(shopItem, amount);
                    if (Server.instance != null)
                    {
                        Server.instance.BuyShopItemSucceed(this, eqType, index, amount);
                    }
                }
                else
                {
                    if (Server.instance != null)
                    {
                        Server.instance.TryBuyShopItemFailed(this, "Notenoughtmoney");
                    }
                    return;
                }
            }
        }
    }


    public void SellShopItem(int index, int amount)
    {
        if (bJailed && Server.instance != null)
        {
            Server.instance.TrySellShopItemFailed(this, "Jailed");
            return;
        }

        // validate: close enough, npc alive and valid index and valid item?
        // use collider point(s) to also work with big entities
        if (0 <= index && index < inventory.Count)
        {
            // sellable?
            ScriptableItemAndAmount slot = inventory[index];
            if (slot.amount > 0 && slot.item.sellable)
            {
                // valid amount?
                if (1 <= amount && amount <= slot.amount)
                {
                    // sell the amount
                    long price = slot.item.sellPrice * amount;
                    //money += (int)price;

                    ChangeCrimePointsMoney(0, (int)price);

                    slot.DecreaseAmount(amount);
                    inventory[index] = slot;

                    if (Server.instance != null)
                    {
                        Server.instance.SellShopItemSucceed(this, index, amount);
                    }
                }
            }
            else
            {
                if (Server.instance != null)
                {
                    Server.instance.TrySellShopItemFailed(this, "notSellable");
                }
                return;
            }
        }
    }

    // helper function to remove 'n' items from the inventory
    public bool InventoryRemove(Item item, int amount)
    {
        for (int i = 0; i < inventory.Count; ++i)
        {
            ScriptableItemAndAmount slot = inventory[i];
            // note: .Equals because name AND dynamic variables matter (petLevel etc.)
            if (slot.amount > 0 && slot.item.Equals(item))
            {
                // take as many as possible
                amount -= slot.DecreaseAmount(amount);
                inventory[i] = slot;

                // are we done?
                if (amount == 0) return true;
            }
        }

        // if we got here, then we didn't remove enough items
        return false;
    }

#endregion

    public bool InventoryRemoveItem(ScriptableItem item, int amount)
    {
        for (int i = 0; i < inventory.Count; ++i)
        {
            ScriptableItemAndAmount slot = inventory[i];
            // note: .Equals because name AND dynamic variables matter (petLevel etc.)
            if (slot.amount > 0 && slot.item.name == item.name)
            {
                // take as many as possible
                amount -= slot.DecreaseAmount(amount);
                inventory[i] = slot;

                // are we done?
                if (amount == 0) return true;
            }
        }

        // if we got here, then we didn't remove enough items
        return false;
    }

    // helper function to calculate the total amount of an item type in inventory
    // note: .Equals because name AND dynamic variables matter (petLevel etc.)
    public int InventoryCount(Item item)
    {
        // count manually. Linq is HEAVY(!) on GC and performance
        int amount = 0;
        foreach (ScriptableItemAndAmount slot in inventory)
            if (slot.amount > 0 && slot.item.Equals(item))
                amount += slot.amount;
        return amount;
    }

    // helper function to calculate the total amount of an item type in inventory
    // note: .Equals because name AND dynamic variables matter (petLevel etc.)
    public int InventoryCountItem(ScriptableItem item)
    {
        // count manually. Linq is HEAVY(!) on GC and performance
        int amount = 0;
        foreach (ScriptableItemAndAmount slot in inventory)
            if (slot.amount > 0 && slot.item.name == item.name)
                amount += slot.amount;
        return amount;
    }

    // helper function to check if the inventory has space for 'n' items of type
    // -> the easiest solution would be to check for enough free item slots
    // -> it's better to try to add it onto existing stacks of the same type
    //    first though
    // -> it could easily take more than one slot too
    // note: this checks for one item type once. we can't use this function to
    //       check if we can add 10 potions and then 10 potions again (e.g. when
    //       doing player to player trading), because it will be the same result
    public bool InventoryCanAdd(Item item, int amount)
    {
        // go through each slot
        for (int i = 0; i < inventory.Count; ++i)
        {
            // empty? then subtract maxstack
            if (inventory[i].amount == 0)
                amount -= item.data.maxStack;
            // not empty. same type too? then subtract free amount (max-amount)
            // note: .Equals because name AND dynamic variables matter (petLevel etc.)
            else if (inventory[i].item.Equals(item))
                amount -= (inventory[i].item.maxStack - inventory[i].amount);

            // were we able to fit the whole amount already?
            if (amount <= 0) return true;
        }

        // if we got here than amount was never <= 0
        return false;
    }

    // helper function to put 'n' items of a type into the inventory, while
    // trying to put them onto existing item stacks first
    // -> this is better than always adding items to the first free slot
    // -> function will only add them if there is enough space for all of them
    public bool InventoryAdd(Item item, int amount)
    {
        // we only want to add them if there is enough space for all of them, so
        // let's double check
        if (InventoryCanAdd(item, amount))
        {
            // add to same item stacks first (if any)
            // (otherwise we add to first empty even if there is an existing
            //  stack afterwards)
            for (int i = 0; i < inventory.Count; ++i)
            {
                // not empty and same type? then add free amount (max-amount)
                // note: .Equals because name AND dynamic variables matter (petLevel etc.)
                if (inventory[i].amount > 0 && inventory[i].item.Equals(item))
                {
                    ScriptableItemAndAmount temp = inventory[i];
                    amount -= temp.IncreaseAmount(amount);
                    inventory[i] = temp;
                }

                // were we able to fit the whole amount already? then stop loop
                if (amount <= 0) return true;
            }

            // add to empty slots (if any)
            for (int i = 0; i < inventory.Count; ++i)
            {
                // empty? then fill slot with as many as possible
                if (inventory[i].amount == 0)
                {
                    int add = Mathf.Min(amount, item.data.maxStack);
                    inventory[i] = new ScriptableItemAndAmount(item.data, add);
                    amount -= add;
                }

                // were we able to fit the whole amount already? then stop loop
                if (amount <= 0) return true;
            }
            // we should have been able to add all of them
            if (amount != 0) Debug.LogError("inventory add failed: " + item.name + " " + amount);
        }
        return false;
    }

    // helper function to put 'n' items of a type into the inventory, while
    // trying to put them onto existing item stacks first
    // -> this is better than always adding items to the first free slot
    // -> function will only add them if there is enough space for all of them
    public bool InventoryAddItem(Item item, int amount)
    {
        // we only want to add them if there is enough space for all of them, so
        // let's double check
        if (InventoryCanAdd(item, amount))
        {
            // add to same item stacks first (if any)
            // (otherwise we add to first empty even if there is an existing
            //  stack afterwards)
            for (int i = 0; i < inventory.Count; ++i)
            {
                // not empty and same type? then add free amount (max-amount)
                // note: .Equals because name AND dynamic variables matter (petLevel etc.)
                if (inventory[i].amount > 0 && inventory[i].item.name == item.name)
                {
                    ScriptableItemAndAmount temp = inventory[i];
                    amount -= temp.IncreaseAmount(amount);
                    inventory[i] = temp;
                }

                // were we able to fit the whole amount already? then stop loop
                if (amount <= 0) return true;
            }

            // add to empty slots (if any)
            for (int i = 0; i < inventory.Count; ++i)
            {
                // empty? then fill slot with as many as possible
                if (inventory[i].amount == 0)
                {
                    int add = Mathf.Min(amount, item.data.maxStack);
                    inventory[i] = new ScriptableItemAndAmount(item.data, add);
                    amount -= add;
                }

                // were we able to fit the whole amount already? then stop loop
                if (amount <= 0) return true;
            }
            // we should have been able to add all of them
            if (amount != 0) Debug.LogError("inventory add failed: " + item.name + " " + amount);
        }
        return false;
    }

    #region Crime

    public void CommitCrime(ScriptableEnemy enemy)
    {
        if(Server.instance == null)
        {
            return;
        }

        if(bJailed)
        {
            Server.instance.TryCommitCrimeFailed(this, "Jailed");
            return;
        }

        if (enemy.rank > lastSucceedStoryNo)
        {
            Server.instance.TryCommitCrimeFailed(this, "Needtodostoryfirst");
            return;
        }
        
        if ((lastAttackTime - GameManager.instance.ServerTime).TotalSeconds > 0)
        {
            Server.instance.TryCommitCrimeFailed(this, "Needtowaitforcrime");
            return;
        }

        if(curHealth < enemy.healthRequirement)
        {
            Server.instance.TryCommitCrimeFailed(this, "Healthisnotenought");
            return;
        }
        else
        {
            curHealth -= enemy.healthRequirement;
        }

        float luckPlayer = intelligence / enemy.intelligence;
        float luckEnemy = enemy.intelligence / intelligence;

        float playerAttackBonus = strength / enemy.strength; playerAttackBonus = Mathf.Max(playerAttackBonus, 1);
        float enemyAttackBonus = enemy.strength / strength; enemyAttackBonus = Mathf.Max(enemyAttackBonus, 1);

        float playerAttackModified = (attack * playerAttackBonus) * luckPlayer - enemy.defense; playerAttackModified = Mathf.Max(playerAttackModified, 1);
        float enemyAttackModified = (enemy.attack * enemyAttackBonus) * luckEnemy - defense; enemyAttackModified = Mathf.Max(enemyAttackModified, 1);

        float randomAttack = UnityEngine.Random.Range(0, playerAttackModified + enemyAttackModified);

        Debug.Log(luckPlayer + " " + luckEnemy + " " + playerAttackBonus + " " + enemyAttackBonus + " " + playerAttackModified + " " + enemyAttackModified + " " + randomAttack);

        float vipBonus = vip.hash != 0 ? vip.data.attackTimeDecreaser : 0;

        lastAttackTime = GameManager.instance.ServerTime + new TimeSpan(0, 0, Mathf.FloorToInt(enemy.attackPeriod * (1 - vipBonus / 100)));

        if (randomAttack <= playerAttackModified)
        {
            Debug.LogError("Player Win");

            int randomMoney = UnityEngine.Random.Range(enemy.moneyMin, enemy.moneyMax);
            //money += randomMoney;

            int randomCrimePoints = UnityEngine.Random.Range(enemy.crimePointMin, enemy.crimePointMax);
            //crimePoints += randomCrimePoints;

            ChangeCrimePointsMoney(randomCrimePoints, randomMoney);

            CheckGrade();

            Server.instance.totalPvESucceed++;

            Server.instance.CommitCrimeSucceed(this, enemy.name.GetStableHashCode());
        }
        else
        {
            Debug.LogError("Enemy Win");

            Server.instance.totalPvEFailed++;

            float jailRate = enemy.jailRate / ((int)playerGrade + 1);

            bool jailed = jailRate >= UnityEngine.Random.Range(0, 99);

            if(jailed)
            {
                vipBonus = vip.hash != 0 ? vip.data.jailTimeDecreaser : 0;

                jailFinishTime = GameManager.instance.ServerTime + TimeSpan.FromSeconds(enemy.jailSeconds * (1 - vipBonus / 100));
                totalPvEDeath++;
            }

            Server.instance.CommitCrimeFailed(this, enemy.name.GetStableHashCode(), jailed);
        }

        //son saldırma süresini ekle bir dahaki saldırıda kontrol et;
    }

    public float CommitCrimeSuccessChance(ScriptableEnemy enemy)
    {
        float luckPlayer = intelligence / enemy.intelligence;
        float luckEnemy = enemy.intelligence / intelligence;

        float playerAttackBonus = strength / enemy.strength; playerAttackBonus = Mathf.Max(playerAttackBonus, 1);
        float enemyAttackBonus = enemy.strength / strength; enemyAttackBonus = Mathf.Max(enemyAttackBonus, 1);

        float playerAttackModified = (attack * playerAttackBonus) * luckPlayer - enemy.defense; playerAttackModified = Mathf.Max(playerAttackModified, 1);
        float enemyAttackModified = (enemy.attack * enemyAttackBonus) * luckEnemy - defense; enemyAttackModified = Mathf.Max(enemyAttackModified, 1);

        return (playerAttackModified * 100) / (playerAttackModified + enemyAttackModified);
    }

    #endregion

    public void CheckGrade()
    {
        for(int i = 0; i < GameManager.instance.gradePoints.Count; i++)
        {
            if(crimePoints >= GameManager.instance.gradePoints[i].point)
            {
                playerGrade = GameManager.instance.gradePoints[i].grade;
            }
        }
    }

    #region Real Estates
    public int GetRealEstateIndexByName(string realEstateName)
    {
        // (avoid FindIndex to minimize allocations)
        for (int i = 0; i < realEstates.Count; ++i)
            if (realEstates[i].name == realEstateName)
                return i;
        return -1;
    }

    public void AddRealEstate(RealEstate realEstate, int count)
    {
        // reset if already in realEstate list, otherwise add
        int index = GetRealEstateIndexByName(realEstate.data.name);
        if (index != -1)
        {
            RealEstate re = realEstates[index];
            re.count += count;
            realEstates[index] = re;

            if (GameAnalyticsService.instance != null)
            {
                GameAnalyticsService.instance.AddProgressionEvent(GameAnalyticsSDK.GAProgressionStatus.Complete, "BuyManagement", 1);
            }
        }
        else
        {
            RealEstate re = realEstate;
            re.count = count;
            realEstates.Add(re);

            if (GameAnalyticsService.instance != null)
            {
                GameAnalyticsService.instance.AddProgressionEvent(GameAnalyticsSDK.GAProgressionStatus.Complete, "BuyManagementFirst", 1);
            }
        }
    }

    public void BuyRealEstate(ScriptableRealEstate realEstate)
    {
        if (Server.instance == null)
        {
            return;
        }

        if (bJailed)
        {
            Server.instance.TryBuyRealEstateFailed(this, "Jailed");
            return;
        }
        
        if (money < realEstate.buyPrice)
        {
            Server.instance.TryBuyRealEstateFailed(this, "Notenoughtmoney");
            return;
        }
        else
        {
            //money -= realEstate.buyPrice;

            ChangeCrimePointsMoney(0, -1 * realEstate.buyPrice);
        }

        AddRealEstate(new RealEstate(realEstate), 1);
        
        Server.instance.totalRealEstateBuy++;

        Server.instance.BuyRealEstateSucceed(this, realEstate.name.GetStableHashCode());
    }

    public void GetIncomeRealEstate(ScriptableRealEstate realEstate)
    {
        if (Server.instance == null)
        {
            return;
        }

        if (bJailed)
        {
            Server.instance.TryBuyRealEstateFailed(this, "Jailed");
            return;
        }

        int realEstatesIndex = GetRealEstateIndexByName(realEstate.name);

        if (realEstatesIndex != -1)
        {
            RealEstate realEstatePlayer = realEstates[realEstatesIndex];
            realEstatePlayer.lastCollectTime = GameManager.instance.ServerTime;
            realEstates[realEstatesIndex] = realEstatePlayer;

            float vipBonus = vip.hash != 0 ? vip.data.incomeIncreaser : 0;

            int income = Mathf.FloorToInt(realEstatePlayer.count * realEstatePlayer.data.income * (1 + vipBonus / 100));

            Server.instance.totalRealEstateIncomeCount++;
            Server.instance.totalRealEstateIncomeMoney += income;

            //money += income;

            ChangeCrimePointsMoney(0, income);

            Server.instance.GetRealEstateIncomeSucceed(this, realEstate.name.GetStableHashCode());
        }
        else
        {
            Server.instance.GetRealEstateIncomeFailed(this, realEstate.name.GetStableHashCode());
        }

    }

    #endregion

    #region Management

    public int GetManagementIndexByName(string managementName)
    {
        // (avoid FindIndex to minimize allocations)
        for (int i = 0; i < managements.Count; ++i)
            if (managements[i].name == managementName)
                return i;
        return -1;
    }

    public void AddManagement(Management management, int count)
    {
        // reset if already in realEstate list, otherwise add
        int index = GetManagementIndexByName(management.data.name);
        if (index != -1)
        {
            Management me = managements[index];
            me.count += count;
            managements[index] = me;

            if (GameAnalyticsService.instance != null)
            {
                GameAnalyticsService.instance.AddProgressionEvent(GameAnalyticsSDK.GAProgressionStatus.Complete, "BuyManagement", 1);
            }
        }
        else
        {
            Management me = management;
            me.count = count;
            managements.Add(me);

            if (GameAnalyticsService.instance != null)
            {
                GameAnalyticsService.instance.AddProgressionEvent(GameAnalyticsSDK.GAProgressionStatus.Complete, "BuyManagementFirst", 1);
            }
        }
    }

    public void BuyManagement(ScriptableManagement management)
    {
        if (Server.instance == null)
        {
            return;
        }

        if (bJailed)
        {
            Server.instance.TryBuyManagementFailed(this, "Jailed");
            return;
        }

        if (money < management.buyPrice)
        {
            Server.instance.TryBuyManagementFailed(this, "Notenoughtmoney");
            return;
        }
        else
        {
            //money -= management.buyPrice;

            ChangeCrimePointsMoney(0, -1 * management.buyPrice);
        }

        AddManagement(new Management(management), 1);

        Server.instance.totalManagementBuy++;

        Server.instance.BuyManagementSucceed(this, management.name.GetStableHashCode());
    }

    public void GetIncomeManagement(ScriptableManagement management)
    {
        if (Server.instance == null)
        {
            return;
        }

        if (bJailed)
        {
            Server.instance.TryBuyManagementFailed(this, "Jailed");
            return;
        }

        int managementsIndex = GetManagementIndexByName(management.name);

        if (managementsIndex != -1)
        {
            Management managementPlayer = managements[managementsIndex];
            managementPlayer.lastCollectTime = GameManager.instance.ServerTime;
            managements[managementsIndex] = managementPlayer;

            float vipBonus = vip.hash != 0 ? vip.data.incomeIncreaser : 0;

            int income = Mathf.FloorToInt(managementPlayer.count * managementPlayer.data.income * (1 + vipBonus / 100));

            Server.instance.totalManagementIncomeCount++;
            Server.instance.totalManagementIncomeMoney += income;

            //money += income;

            ChangeCrimePointsMoney(0, income);

            Server.instance.GetManagementIncomeSucceed(this, management.name.GetStableHashCode());
        }
        else
        {
            Server.instance.GetManagementIncomeFailed(this, management.name.GetStableHashCode());
        }
    }
    #endregion

    #region Train

    public void Train(string trainType)
    {
        if (Server.instance == null)
        {
            return;
        }

        if (bJailed)
        {
            Server.instance.TryTrainFailed(this, "Jailed");
            return;
        }

        int outcome = 0;
        int gain = 0;
        int healthRequirement = 0;

        if (trainType == "STR")
        {
            gain = GameManager.instance.trainGainRates[Mathf.Min((int)Mathf.Floor(strength / 100f), 5)];
            outcome = GameManager.instance.trainMoneyRates[Mathf.Min((int)Mathf.Floor(strength / 100f), 5)];
            healthRequirement = GameManager.instance.trainHealthRates[Mathf.Min((int)Mathf.Floor(strength / 100f), 5)];
        }
        else if (trainType == "INT")
        {
            gain = GameManager.instance.trainGainRates[Mathf.Min((int)Mathf.Floor(intelligence / 100f), 5)];
            outcome = GameManager.instance.trainMoneyRates[Mathf.Min((int)Mathf.Floor(intelligence / 100f), 5)];
            healthRequirement = GameManager.instance.trainHealthRates[Mathf.Min((int)Mathf.Floor(intelligence / 100f), 5)];
        }

        if (money < outcome)
        {
            Server.instance.TryTrainFailed(this, "Notenoughtmoney");
            return;
        }
        else
        {
            if (curHealth < healthRequirement)
            {
                Server.instance.TryTrainFailed(this, "Healthisnotenought");
                return;
            }
            else
            {
                curHealth -= healthRequirement;
            }
            //money -= outcome;

            ChangeCrimePointsMoney(0, -1 * outcome);
        }

        if (trainType == "STR")
        {
            _strength += gain;
            Server.instance.totalTrainingStrengthIncome += gain;
        }
        else if (trainType == "INT")
        {
            _intelligence += gain;
            Server.instance.totalTrainingIntelligenceIncome += gain;
        }


        Server.instance.totalTrained++;
        Server.instance.totalTrainingOutcome += outcome;

        Server.instance.TrainSucceed(this, trainType);
        //Server.instance.TrainFailed(this, trainType);
    }
    #endregion

    #region Prison

    public void GiveABribe()
    {
        if (Server.instance == null)
        {
            return;
        }

        if (!bJailed)
        {
            Server.instance.TryGiveABribeFailed(this, "notJailed");
            return;
        }

        if (money < GetBribeMoney())
        {
            Server.instance.TryGiveABribeFailed(this, "Notenoughtmoney");
            return;
        }
        else
        {
            //money -= GetBribeMoney();

            ChangeCrimePointsMoney(0, -1 * GetBribeMoney());
        }

        int randomBribe = UnityEngine.Random.Range(0, 99);

        if(randomBribe > GameManager.instance.bribeSuccessRate)
        {
            jailFinishTime = GameManager.instance.ServerTime;

            Server.instance.GiveABribeSucceed(this);
        }
        else
        {
            Server.instance.GiveABribeFailed(this, "");
        }
    }

    public int GetBribeMoney()
    {
        float money = 0;

        float timeRemaining = (float)(jailFinishTime - GameManager.instance.ServerTime).TotalSeconds;

        money = GameManager.instance.baseMoneyJailSeconds * (intelligence / 100) * (strength / 50) * timeRemaining;

        return Mathf.FloorToInt(money);
    }

    public void EscapeFromPrison()
    {
        if (Server.instance == null)
        {
            return;
        }

        if (!bJailed)
        {
            Server.instance.TryEscapeFromPrisonFailed(this, "notJailed");
            return;
        }

        int randomEscape = UnityEngine.Random.Range(0, 99);

        if (randomEscape > GameManager.instance.escapeFromPrisonRate)
        {
            jailFinishTime = GameManager.instance.ServerTime;

            Server.instance.EscapeFromPrisonSucceed(this);
        }
        else
        {
            Server.instance.EscapeFromPrisonFailed(this, "");
        }
    }

    #endregion

    #region Bank
    public void PutMoneyBank(bool fromBankToInventory, int _money)
    {
        if (Server.instance == null)
        {
            return;
        }

        if (bJailed)
        {
            Server.instance.TryGiveABribeFailed(this, "Jailed");
            return;
        }

        float vipBonus = vip.hash != 0 ? vip.data.maxMoneyBankIncreaser : 0;

        int totalMoney = money + moneyBank;
        int maxBankMoney = Mathf.FloorToInt(totalMoney *  (50 + vipBonus) / 100);

        if(fromBankToInventory)
        {
            if (moneyBank < _money)
            {
                Server.instance.TryMoneyBankTransferFailed(this, "Notenoughtmoney");
            }
            else
            {
                moneyBank -= _money;
                //money += _money;

                ChangeCrimePointsMoney(0, _money);

                Server.instance.MoneyBankTransferSucceed(this);
            }
        }
        else
        {
            if (moneyBank >= maxBankMoney || _money + moneyBank > maxBankMoney)
            {
                Server.instance.TryMoneyBankTransferFailed(this, "bankMoneyMax");
                return;
            }

            if(money < _money)
            {
                Server.instance.TryMoneyBankTransferFailed(this, "Notenoughtmoney");
            }
            else
            {
                //money -= _money;
                moneyBank += _money;

                ChangeCrimePointsMoney(0, -1 * _money);

                Server.instance.MoneyBankTransferSucceed(this);
            }
        }   
    }
    #endregion

    #region Announcements

    public void RefreshAnnouncements()
    {
        UIManager.instance.RefreshAnnouncements();
    }

    #endregion

    #region Story

    public void DoStory(ScriptableStory story)
    {
        Debug.LogError("Do Story");
        if (Server.instance == null)
        {
            return;
        }

        if (bJailed)
        {
            Server.instance.TryDoStoryFailed(this, "Jailed");
            return;
        }

        if (story.storyNo != lastSucceedStoryNo + 1)
        {
            Server.instance.TryDoStoryFailed(this, "Continuewhereyouleft");
            return;
        }

        if (curHealth < story.healthRequirement)
        {
            Server.instance.TryDoStoryFailed(this, "Healthisnotenought");
            return;
        }
        else
        {
            curHealth -= story.healthRequirement;
        }

        float luckPlayer = intelligence / story.intelligence;
        float luckEnemy = story.intelligence / intelligence;

        float playerAttackBonus = strength / story.strength; playerAttackBonus = Mathf.Max(playerAttackBonus, 1);
        float enemyAttackBonus = story.strength / strength; enemyAttackBonus = Mathf.Max(enemyAttackBonus, 1);

        float playerAttackModified = (attack * playerAttackBonus) * luckPlayer - story.defense; playerAttackModified = Mathf.Max(playerAttackModified, 1);
        float enemyAttackModified = (story.attack * enemyAttackBonus) * luckEnemy - defense; enemyAttackModified = Mathf.Max(enemyAttackModified, 1);

        float randomAttack = UnityEngine.Random.Range(0, playerAttackModified + enemyAttackModified);

        Debug.Log(luckPlayer + " " + luckEnemy + " " + playerAttackBonus + " " + enemyAttackBonus + " " + playerAttackModified + " " + enemyAttackModified + " " + randomAttack);

        float vipBonus = vip.hash != 0 ? vip.data.attackTimeDecreaser : 0;

        if (randomAttack <= playerAttackModified)
        {
            Debug.LogError("Player Win");

            int randomMoney = UnityEngine.Random.Range(story.moneyMin, story.moneyMax); ;
            //money += randomMoney;

            int randomCrimePoints = UnityEngine.Random.Range(story.crimePointMin, story.crimePointMax);
            //crimePoints += randomCrimePoints;

            ChangeCrimePointsMoney(randomCrimePoints, randomMoney);

            CheckGrade();

            Server.instance.totalPvESucceed++;
            totalPvEKill++;

            lastSucceedStoryNo++;

            Server.instance.DoStorySucceed(this, story.name.GetStableHashCode());
        }
        else
        {
            Debug.LogError("Enemy Win");

            Server.instance.totalPvEFailed++;
            totalPvEDeath++;

            Server.instance.DoStoryFailed(this, story.name.GetStableHashCode());
        }
    }

    public float StorySuccessChance(ScriptableStory story)
    {
        float luckPlayer = intelligence / story.intelligence;
        float luckEnemy = story.intelligence / intelligence;

        float playerAttackBonus = strength / story.strength; playerAttackBonus = Mathf.Max(playerAttackBonus, 1);
        float enemyAttackBonus = story.strength / strength; enemyAttackBonus = Mathf.Max(enemyAttackBonus, 1);

        float playerAttackModified = (attack * playerAttackBonus) * luckPlayer - story.defense; playerAttackModified = Mathf.Max(playerAttackModified, 1);
        float enemyAttackModified = (story.attack * enemyAttackBonus) * luckEnemy - defense; enemyAttackModified = Mathf.Max(enemyAttackModified, 1);

        return (playerAttackModified * 100) / (playerAttackModified + enemyAttackModified);
    }

    #endregion

    #region Smuggling

    public float GetRemainingTimeForChangeSmugglingItemPrice()
    {
        if ((GameManager.instance.ServerTime - nextChangeTimeSmugglintItemsPrices).TotalHours > 22)
        {
            return 0;
        }
        else
        {
            if (GameManager.instance.ServerTime.Hour / 2 != nextChangeTimeSmugglintItemsPrices.Hour / 2)
            {
                return 0;
            }
            else
            {
                int nextChangeHour = (2 * (1 + (int)Math.Floor((double)GameManager.instance.ServerTime.Hour / 2)));

                DateTime nextChangeTime = GameManager.instance.ServerTime.AddHours(nextChangeHour - GameManager.instance.ServerTime.Hour)
                    .AddMinutes(-1 * GameManager.instance.ServerTime.Minute)
                    .AddSeconds(-1 * GameManager.instance.ServerTime.Second)
                    .AddMilliseconds(-1 * GameManager.instance.ServerTime.Millisecond);

                return (float)(nextChangeTime - GameManager.instance.ServerTime).TotalSeconds;
            }
        }
    }

    public void BuySmugglingItem(int index, int amount = 1)
    {
        if (bJailed && Server.instance != null)
        {
            Server.instance.TryBuySmugglingItemsFailed(this, "Jailed");
            return;
        }

        List<ScriptableItemAndPrice> _smugglingItems = new List<ScriptableItemAndPrice>();


        if (Server.instance != null)
        {
            _smugglingItems = Server.instance.smugglingItems;
        }
        else
        {
            _smugglingItems = smugglingItems;
        }

        if (0 <= index && index < _smugglingItems.Count)
        {
            Item smugglingItem = new Item(ScriptableItem.dict[_smugglingItems[index].hash]);
            
            // valid amount?
            if (1 <= amount)
            {
                long price = _smugglingItems[index].price * amount;

                // enough gold and enough space in inventory?
                if (money >= price && InventoryCanAdd(smugglingItem, amount))
                {
                    // pay for it, add to inventory
                    //money -= (int)price;

                    ChangeCrimePointsMoney(0, -1 * (int)price);

                    InventoryAddItem(smugglingItem, amount);

                    if (Server.instance != null)
                    {
                        Server.instance.BuySmugglingItemsSucceed(this, index, amount);
                    }
                }
                else
                {
                    if (Server.instance != null)
                    {
                        Debug.LogError(5);
                        Server.instance.BuySmugglingItemsFailed(this, "Notenoughtmoney");
                    }
                    return;
                }
            }
        }
    }


    public void SellSmugglingItem(int index, int amount)
    {
        if (bJailed && Server.instance != null)
        {
            Server.instance.TrySellSmugglingItemsFailed(this, "Jailed");
            return;
        }

        List<ScriptableItemAndPrice> _smugglingItems = new List<ScriptableItemAndPrice>();

        if (Server.instance != null)
        {
            _smugglingItems = Server.instance.smugglingItems;
        }
        else
        {
            _smugglingItems = smugglingItems;
        }

        // validate: close enough, npc alive and valid index and valid item?
        // use collider point(s) to also work with big entities
        if (0 <= index && index < _smugglingItems.Count)
        {
            // sellable?
            ScriptableItemAndPrice smugglingItem = _smugglingItems[index];
            ScriptableItem _smugglingItem = ScriptableItem.dict[smugglingItem.hash];

            if (_smugglingItem is SmugglingItem)
            {
                // valid amount?
                if (1 <= amount && amount <= InventoryCountItem(_smugglingItem))
                {
                    // sell the amount
                    long price = smugglingItem.price * amount;
                    //money += (int)price;

                    ChangeCrimePointsMoney(0, (int)price);

                    InventoryRemoveItem(ScriptableItem.dict[_smugglingItems[index].hash], amount);

                    if (Server.instance != null)
                    {
                        Server.instance.SellSmugglingItemsSucceed(this, index, amount);
                    }

                    if (GameAnalyticsService.instance != null)
                    {
                        GameAnalyticsService.instance.AddProgressionEvent(GameAnalyticsSDK.GAProgressionStatus.Complete, "SmugglingItem", amount);
                    }
                }
                else
                {
                    if (Server.instance != null)
                    {
                        Server.instance.SellSmugglingItemsFailed(this, "notenoughtcount");
                    }
                }
            }
            else
            {
                if (Server.instance != null)
                {
                    Server.instance.SellSmugglingItemsFailed(this, "notSellable");
                }
                return;
            }
        }
    }
    #endregion

    #region Arena

    public void ArenaAttack(Player enemy)
    {
        if (Server.instance == null)
        {
            return;
        }

        if (bJailed)
        {
            Server.instance.TryArenaAttackFailed(this, "Jailed");
            return;
        }

        if ((lastArenaAttackTime - GameManager.instance.ServerTime).TotalSeconds > 0)
        {
            Server.instance.TryArenaAttackFailed(this, "Needtowaitforarena");
            return;
        }

        int healthRequirement = GameManager.instance.arenaHealthRequirement;

        if (curHealth < healthRequirement)
        {
            Server.instance.TryArenaAttackFailed(this, "Healthisnotenought");
            return;
        }
        else
        {
            curHealth -= healthRequirement;
        }

        float luckPlayer = intelligence / enemy.intelligence;
        float luckEnemy = enemy.intelligence / intelligence;

        float playerAttackBonus = strength / enemy.strength; playerAttackBonus = Mathf.Max(playerAttackBonus, 1);
        float enemyAttackBonus = enemy.strength / strength; enemyAttackBonus = Mathf.Max(enemyAttackBonus, 1);

        float playerAttackModified = (attack * playerAttackBonus) * luckPlayer - enemy.defense; playerAttackModified = Mathf.Max(playerAttackModified, 1);
        float enemyAttackModified = (enemy.attack * enemyAttackBonus) * luckEnemy - defense; enemyAttackModified = Mathf.Max(enemyAttackModified, 1);

        float randomAttack = UnityEngine.Random.Range(0, playerAttackModified + enemyAttackModified);

        Debug.Log(luckPlayer + " " + luckEnemy + " " + playerAttackBonus + " " + enemyAttackBonus + " " + playerAttackModified + " " + enemyAttackModified + " " + randomAttack);

        float vipBonus = vip.hash != 0 ? vip.data.arenaAttackTimeDecreaser : 0;

        lastArenaAttackTime = GameManager.instance.ServerTime + new TimeSpan(0, 0, Mathf.FloorToInt(GameManager.instance.arenaAttackPeriod * (1 - vipBonus / 100)));

        if (randomAttack <= playerAttackModified)
        {
            Debug.LogError("Player Win");

            int earnedMoney = Mathf.FloorToInt(enemy.money / 4);
            //money += earnedMoney;
            //enemy.money -= earnedMoney;

            int earnedCrimePoints = GameManager.instance.arenaWinCrimePoints;
            //crimePoints += earnedCrimePoints;
            //enemy.crimePoints -= earnedCrimePoints;

            ChangeCrimePointsMoney(earnedCrimePoints, earnedMoney);

            enemy.ChangeCrimePointsMoney(-1 * earnedCrimePoints, -1 * earnedMoney);

            CheckGrade();
            enemy.CheckGrade();

            Server.instance.totalPvPSucceed++;

            Server.instance.ArenaAttackSucceed(this, enemy);
            totalArenaKill++;

            //Server.instance.ArenaAttackFailed(enemy, this);
            enemy.totalArenaDeath++;

            Server.instance.UserSave(this);
            Server.instance.UserSave(enemy);

            Mail enemyMail = new Mail(-1, "{system}", enemy.charName, "{arena}", charName + " " + "{killedyouinarena}" + " - " + earnedMoney, new List<ScriptableItemAndAmount>(), false);

            Server.instance.SaveMail(enemyMail);

            if (enemy.online)
            {
                Server.instance.SyncMoney(enemy);

                enemy.mails = Server.instance.GetPlayerMails(enemy);
                Server.instance.SyncMail(enemy);
            }
        }
        else
        {
            Debug.LogError("Enemy Win");

            int earnedMoney = 0; //Mathf.FloorToInt(money / 4);

            //money -= earnedMoney;
            //enemy.money += earnedMoney;

            ChangeCrimePointsMoney(0, -1 * earnedMoney);

            int earnedCrimePoints = GameManager.instance.arenaWinCrimePoints;
            crimePoints -= earnedCrimePoints;
            //enemy.crimePoints += earnedCrimePoints;

            enemy.ChangeCrimePointsMoney(earnedCrimePoints, earnedMoney);

            enemy.CheckGrade();
            CheckGrade();

            Server.instance.totalPvPFailed++;

            //Server.instance.ArenaAttackSucceed(enemy, this);
            enemy.totalArenaKill++;

            Server.instance.ArenaAttackFailed(this, enemy);
            totalArenaDeath++;

            Server.instance.UserSave(this);
            Server.instance.UserSave(enemy);

            Mail enemyMail = new Mail(-1, "{system}", enemy.charName, "{arena}", charName + " " + "{istriedtokillyouinarena}", new List<ScriptableItemAndAmount>(), false);

            Server.instance.SaveMail(enemyMail);

            if (enemy.online)
            {
                enemy.mails = Server.instance.GetPlayerMails(enemy);
                Server.instance.SyncMail(enemy);
            }
        }

        //son saldırma süresini ekle bir dahaki saldırıda kontrol et;
    }

    #endregion

    #region Chat

    public void SendChatMessage(string identifier, string message, string replyPrefix)
    {
        ChatMessage cm = new ChatMessage(charName, identifier, message, replyPrefix, 0);

        MafiasCrimeWorldClient.instance.SendChatMessage(cm);
    }

    #endregion



    #region Mails

    public void GetItemsFromMail(int id)
    {
        int mailID = mails.FindIndex(x => x.id == id);

        if(mailID != -1)
        {
            var items = mails[mailID].items;

            if(inventory.Where(x => x.amount == 0).ToList().Count > items.Count)
            {
                for(int no = 0; no < items.Count; no++)
                {
                    InventoryAddItem(new Item(items[no].item) , items[no].amount);
                }

                Server.instance.GetItemsFromMailSucceed(this);
            }
            else
            {
                Server.instance.GetItemsFromMailFailed(this, "notenoughtspacebag");
            }
        }
        else
        {
            Server.instance.GetItemsFromMailFailed(this, "mailisnotfound");
        }
    }

    #endregion  
    
}

public enum PlayerGrade
{
    Grade0 = 0,
    Grade1 = 1,
    Grade2 = 2,
    Grade3 = 3,
    Grade4 = 4,
    Grade5 = 5,
    Grade6 = 6,
    Grade7 = 7,
    Grade8 = 8,
    Grade9 = 9,
    Grade10 = 10,
    Grade11 = 11,
    Grade12 = 12,
    Grade13 = 13,
    Grade14 = 14,
    Grade15 = 15,
}

[System.Serializable]
public partial struct EquipmentInfo
{
    public PlayerEquipmentTypes requiredCategory;
    public ScriptableItemAndAmount defaultItem;
}