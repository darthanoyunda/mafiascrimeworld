﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public static GameManager instance;

    public DateTime serverTime = DateTime.UtcNow;

    public TimeSpan serverMinusClientTimeSpan = new TimeSpan();

    public DateTime ServerTime
    {
        get {
            if(Server.instance != null)
            {
                return DateTime.UtcNow;
            }
            else
            {
                return DateTime.UtcNow + serverMinusClientTimeSpan;
            }
        }
    }

    [Header("Account")]
    public int minPasswordLenght = 6;
    public int playerInventorySize = 15;

    [Header("Player Health")]
    public float regenerationMinute = 10;
    public int regenerationHealth = 5;

    [Header("Arena")]
    public int arenaHealthRequirement = 10;
    public float arenaAttackPeriod = 300;
    public int arenaWinCrimePoints = 15;

    [Header("Others")]
    public float taxCollectPeriod = 6; //as hours
    public List<GradePoint> gradePoints;


    [Header("Train")]
    /// <summary>
    /// Shooting Range Rates
    /// </summary>
    public List<int> trainGainRates;
    public List<int> trainMoneyRates;
    public List<int> trainHealthRates;

    [Header("Jail")]
    public float baseMoneyJailSeconds = 3;
    public int bribeSuccessRate = 29; //29 mean %70
    public int escapeFromPrisonRate = -1; // 0 mean %100

    [Header("Chat")]
    public ChannelInfo whisper = new ChannelInfo("/w", "(TO)", "(FROM)");
    public ChannelInfo local = new ChannelInfo("", "", "");
    public ChannelInfo guild = new ChannelInfo("/g", "(Guild)", "(Guild)");
    public ChannelInfo info = new ChannelInfo("", "(Info)", "(Info)");

    [Header("Guild")]
    // configuration
    public int GuildCapacity = 10;
    public int GuildNoticeMaxLength = 50;
    public int GuildCreationPrice = 10000;
    public int GuildNameMaxLength = 20;

    public GuildRank GuildInviteMinRank = GuildRank.Vice;
    public GuildRank GuildKickMinRank = GuildRank.Vice;
    public GuildRank GuildPromoteMinRank = GuildRank.Master; // includes Demote
    public GuildRank GuildNotifyMinRank = GuildRank.Vice;

    [Header("Player Start Datas")]
    public int playerStartMoney = 1500;

    [Header("Steal money Attack Time")]
    public float stealMoneyAttackTime = 900;
    
    [Header("Energy")]
    public int freeEnergy = 20;
    public int freeMoneyRatio = 1; // 1 mean 2x income

    [Header("Smuggling Item Price Change Minutes")]
    public int smugglingItemChangeMinMinute = 120;
    public int smugglingItemChangeMaxMinute = 240;

    private void Awake()
    {
        instance = this;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}


[System.Serializable]
public class GradePoint
{
    public PlayerGrade grade;
    public int point;
}

public enum PlayerEquipmentTypes
{
    None = 0,
    Weapon = 1,
    Head = 2,
    Chest = 3,
    Hands = 4,
    Legs = 5,
    Feet= 6,
    Bag = 7,
}



[Serializable]
public class ChannelInfo
{
    public string command; // /w etc.
    public string identifierOut; // for sending
    public string identifierIn; // for receiving

    public ChannelInfo(string command, string identifierOut, string identifierIn)
    {
        this.command = command;
        this.identifierOut = identifierOut;
        this.identifierIn = identifierIn;
    }
}