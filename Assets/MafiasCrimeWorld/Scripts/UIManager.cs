﻿using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CodeStage.AntiCheat.ObscuredTypes;
using CodeStage.AntiCheat.Storage;

public class UIManager : MonoBehaviour {

    public static UIManager instance;

    public Sprite topButtonActiveSprite;
    public static UITopMenuButton lastClickedTopMenuButton;
    
    public Button buttonFreeEnergy;

    /// <summary>
    /// 
    /// </summary>
    public GameObject LoginRegisterPanel;

    public InputField LoginMail;
    public InputField LoginPassword;

    public Toggle rememberMeToggle;

    public InputField RegisterMail;
    public InputField RegisterMailRe;
    public InputField RegisterPassword;
    public InputField RegisterPasswordRe;

    /// <summary>
    /// 
    /// </summary>

    public GameObject SetCharacterNamePanel;

    public InputField CharacterNameSet;


    /// <summary>
    /// Top Bar
    /// </summary>

    public GameObject TopBarPanel;

    public Text gradeText;
    public Slider gradeSlider;
    public Text crimePointsText;

    public Slider healthSlider;
    public Text healthText;
    public Text healthRegenerationtimeText;

    public Text strenghtText;
    public Text intelligenceText;

    public Text moneyText;

    public Text attackText;
    public Text defenseText;

    public Animator moneyAnimator;
    public Animator crimePointsAnimator;
    public Animator strAnimator;
    public Animator intAnimator;

    /// <summary>
    /// Main Menu
    /// </summary>
    public Button MainMenuButton;
    public GameObject MainMenuPanel;

    /// <summary>
    /// Crime Menu
    /// </summary>
    public GameObject CrimeMenuPanel;

    /// <summary>
    /// Real Estate Menu
    /// </summary>
    public GameObject RealEstateMenuPanel;

    /// <summary>
    /// Management Menu
    /// </summary>
    public GameObject ManagementMenuPanel;

    /// <summary>
    /// Shooting Range
    /// </summary>
    public GameObject ShootingRangePanel;

    /// <summary>
    /// Prison Panel
    /// </summary>
    public GameObject PrisonPanel;

    /// <summary>
    /// Bank Panel
    /// </summary>
    public GameObject BankPanel;

    /// <summary>
    /// Announcements Panel
    /// </summary>
    public GameObject AnnouncementsPanel;
    public Transform announementsContent;
    public UIAnnouncement announcementPrefab;

    /// <summary>
    /// Story Menu
    /// </summary>
    public GameObject StoryPanel;

    /// <summary>
    /// Inventory Menu
    /// </summary>
    public GameObject InventoryMenuPanel;

    /// <summary>
    /// Shop Menu
    /// </summary>
    public GameObject ShopMenuPanel;

    /// <summary>
    /// Smuggling Menu
    /// </summary>
    public GameObject SmugglingMenuPanel;

    /// <summary>
    /// Arena Menu
    /// </summary>
    public GameObject ArenaMenuPanel;

    /// <summary>
    /// Chat Menu
    /// </summary>
    public GameObject ChatMenuPanel;

    /// <summary>
    /// Profile Menu
    /// </summary>
    public GameObject ProfileMenuPanel;

    /// <summary>
    /// Guild Menu
    /// </summary>
    public GameObject GuildMenuPanel;

    /// <summary>
    /// Mail Menu
    /// </summary>
    public GameObject MailMenuPanel;
    
    /// <summary>
    /// Steal Menu
    /// </summary>
    public GameObject StealMenuPanel;

    /// <summary>
    /// VIP Menu
    /// </summary>
    public GameObject VIPMenuPanel;


    public void SetLanguage(string languageCode)
    {
        LocalizationManager.CurrentLanguageCode = languageCode;
        PlayerPrefs.SetString("CurrentLanguageCode", languageCode);
    }

    private void Awake()
    {
        instance = this;

        if(ObscuredPrefs.GetBool("rememberMePF"))
        {
            ObscuredString userNameString = ObscuredPrefs.GetString("userNameStringPF");
            ObscuredString passwordString = ObscuredPrefs.GetString("passwordStringPF");

            rememberMeToggle.isOn = true;
        }
    }

    public void SaveUserNameAndPasswordToPlayerPrefs(bool rememberMe, string username, string password)
    {
        ObscuredPrefs.SetBool("rememberMePF", rememberMe);
        if (rememberMe)
        {
            ObscuredPrefs.SetString("userNameStringPF", username);
            ObscuredPrefs.SetString("passwordStringPF", password);
        }
        else
        {
            ObscuredPrefs.SetString("userNameStringPF", "");
            ObscuredPrefs.SetString("passwordStringPF", "");
        }
    }

    public void LateUpdate()
    {
        if (Player.instance == null)
            return;

        Player player = Player.instance;

        gradeText.text = player.playerGrade.ToString();

        gradeSlider.value = (float)player.crimePoints / GameManager.instance.gradePoints[Mathf.Min((int)player.playerGrade + 1, GameManager.instance.gradePoints.Count -1)].point;

        crimePointsText.text = player.crimePoints.ToString() + "/" + GameManager.instance.gradePoints[Mathf.Min((int)player.playerGrade + 1, GameManager.instance.gradePoints.Count - 1)].point;

        healthSlider.value = (float)player.curHealth / (float)player.health;
        healthText.text = player.curHealth.ToString();

        if(player.curHealth < 100)
        {
            healthRegenerationtimeText.text = TimeSpan.FromSeconds(player.healthRegenerationTime).Minutes.ToString() + " " + TimeSpan.FromSeconds(player.healthRegenerationTime).Seconds.ToString();
            healthRegenerationtimeText.gameObject.SetActive(true);
        }
        else
        {
            healthRegenerationtimeText.gameObject.SetActive(false);
        }

        strenghtText.text = player.strength.ToString();
        intelligenceText.text = player.intelligence.ToString();

        moneyText.text = player.money.ToString();

        attackText.text = player.attack.ToString();
        defenseText.text = player.defense.ToString();

        buttonFreeEnergy.gameObject.SetActive(Player.instance.GetEnergyTimeRemaining() <= 0);
    }

    public void CloseAllPanels()
    {
        OpenTopBarPanel(false);
        CloseMenuPanels();
    }
    public void CloseMenuPanels()
    {
        LoginRegisterPanel.SetActive(false);
        SetCharacterNamePanel.SetActive(false);
        MainMenuPanel.SetActive(false);

        CrimeMenuPanel.SetActive(false);
        RealEstateMenuPanel.SetActive(false);
        ManagementMenuPanel.SetActive(false);

        ShootingRangePanel.SetActive(false);
        PrisonPanel.SetActive(false);
        BankPanel.SetActive(false);

        AnnouncementsPanel.SetActive(false);
        StoryPanel.SetActive(false);
        InventoryMenuPanel.SetActive(false);
        ShopMenuPanel.SetActive(false);
        SmugglingMenuPanel.SetActive(false);
        ArenaMenuPanel.SetActive(false);

        ChatMenuPanel.SetActive(false);
        ProfileMenuPanel.SetActive(false);
        GuildMenuPanel.SetActive(false);
        MailMenuPanel.SetActive(false);
        StealMenuPanel.SetActive(false);
        VIPMenuPanel.SetActive(false);

        if (lastClickedTopMenuButton != null)
        {
           lastClickedTopMenuButton.RefreshSprite();
        }
    }



    #region Login-Register
    public void Login()
    {
        MafiasCrimeWorldClient.instance.LoginRequest(LoginMail.text, LoginPassword.text);

        if(rememberMeToggle.isOn)
        {
            SaveUserNameAndPasswordToPlayerPrefs(rememberMeToggle.isOn, LoginMail.text, LoginPassword.text);
        }
        else
        {
            SaveUserNameAndPasswordToPlayerPrefs(false, "", "");
        }
    }

    public void Register()
    {
        MafiasCrimeWorldClient.instance.RegisterRequest(RegisterMail.text, RegisterMailRe.text, RegisterPassword.text, RegisterPasswordRe.text);
    }

    public void OpenLoginRegisterPanel(bool open)
    {
        LoginRegisterPanel.SetActive(open);

        if (open)
        {
            if (rememberMeToggle.isOn)
            {
                LoginMail.text = ObscuredPrefs.GetString("userNameStringPF");
                LoginPassword.text = ObscuredPrefs.GetString("passwordStringPF");

                RegisterMail.text = "";
                RegisterMailRe.text = "";
                RegisterPassword.text = "";
                RegisterPasswordRe.text = "";
            }
            else
            {
                LoginMail.text = "";
                LoginPassword.text = "";

                RegisterMail.text = "";
                RegisterMailRe.text = "";
                RegisterPassword.text = "";
                RegisterPasswordRe.text = "";
            }
               
        }
    }
    #endregion

    #region SetCharacterName
    public void SetCharacterName()
    {
        MafiasCrimeWorldClient.instance.SetCharnameRequest(CharacterNameSet.text);

        OpenSetCharacterNamePanel(false);

    }
    public void OpenSetCharacterNamePanel(bool open)
    {
        SetCharacterNamePanel.SetActive(open);

        if (open)
        {
            CharacterNameSet.text = "";
        }
    }
    #endregion

    #region TopBar
    public void OpenTopBarPanel(bool open)
    {
        TopBarPanel.SetActive(open);
    }
    #endregion

    #region MainMenu
    public void OpenMainMenuPanel(bool open)
    {
        CloseMenuPanels();
        MainMenuPanel.SetActive(open);

        lastClickedTopMenuButton = MainMenuButton.GetComponent<UITopMenuButton>();
        MainMenuButton.GetComponent<Button>().image.sprite = topButtonActiveSprite;
    }
    #endregion

    #region CrimeMenu
    public void OpenCrimeMenuPanel(bool open)
    {
        CloseMenuPanels();
        CrimeMenuPanel.SetActive(open);
    }
    #endregion

    #region Real Estate
    public void OpenRealEstateMenuPanel(bool open)
    {
        CloseMenuPanels();
        RealEstateMenuPanel.SetActive(open);
    }
    #endregion

    #region Management
    public void OpenManagementMenuPanel(bool open)
    {
        CloseMenuPanels();
        ManagementMenuPanel.SetActive(open);
    }
    #endregion

    #region Shooting Range
    public void OpenShootingRangePanel(bool open)
    {
        CloseMenuPanels();
        ShootingRangePanel.SetActive(open);
    }
    #endregion

    #region Prison
    public void OpenPrisonPanel(bool open)
    {
        CloseMenuPanels();
        PrisonPanel.SetActive(open);
    }
    #endregion
    
    #region Bank
    public void OpenBankPanel(bool open)
    {
        CloseMenuPanels();
        BankPanel.SetActive(open);
    }
    #endregion

    #region Announcements

    public void RefreshAnnouncements()
    {
        UIUtils.BalancePrefabs(announcementPrefab.gameObject, Player.instance.announcements.Count, announementsContent);

        for (int i = 0; i < Player.instance.announcements.Count; ++i)
        {
            UIAnnouncement slot = announementsContent.GetChild(i).GetComponent<UIAnnouncement>();
            slot.announcementNo = i;
        }
    }

    public void OpenAnnouncementPanel(bool open)
    {
        CloseMenuPanels();
        AnnouncementsPanel.SetActive(open);
    }

    #endregion

    #region Story Menu
    public void OpenStoryPanel(bool open)
    {
        CloseMenuPanels();
        StoryPanel.SetActive(open);
    }
    #endregion

    #region Inventory Menu
    public void OpenInventoryMenuPanel(bool open)
    {
        CloseMenuPanels();
        InventoryMenuPanel.SetActive(open);
    }
    #endregion

    #region Shop Menu
    public void OpenShopMenuPanel(bool open)
    {
        CloseMenuPanels();
        ShopMenuPanel.SetActive(open);
    }
    #endregion

    #region Smuggling Menu
    public void OpenSmugglingMenuPanel(bool open)
    {
        CloseMenuPanels();
        SmugglingMenuPanel.SetActive(open);
    }
    #endregion  
    
    #region Arena Menu
    public void OpenArenaMenuPanel(bool open)
    {
        CloseMenuPanels();
        ArenaMenuPanel.SetActive(open);

        MafiasCrimeWorldClient.instance.GetArenaListRequest();
    }
    #endregion    
    
    #region Arena Menu
    public void OpenChatMenuPanel(bool open)
    {
        CloseMenuPanels();
        ChatMenuPanel.SetActive(open);
    }
    #endregion

    #region Profile Menu
    public void OpenProfileMenuPanel(bool open)
    {
        CloseMenuPanels();
        ProfileMenuPanel.SetActive(open);
    }
    #endregion  
    
    #region Guild Menu
    public void OpenGuildMenuPanel(bool open)
    {
        CloseMenuPanels();
        GuildMenuPanel.SetActive(open);
    }
    #endregion
    
    #region Mail Menu
    public void OpenMailMenuPanel(bool open)
    {
        CloseMenuPanels();
        MailMenuPanel.SetActive(open);
    }
    #endregion

    #region Steal Money Menu
    public void OpenStealMoneyMenuPanel(bool open)
    {
        CloseMenuPanels();
        StealMenuPanel.SetActive(open);
    }
    #endregion

    #region VIP Menu
    public void OpenVIPMenuPanel(bool open)
    {
        CloseMenuPanels();
        VIPMenuPanel.SetActive(open);
    }
    #endregion

    public void GetFreeEnergyRequest()
    {
        if(Player.instance.GetEnergyTimeRemaining() <= 0)
        {
#if UNITY_EDITOR
            CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                                     LocalizationManager.GetTermTranslation("unexpectedPlatform"),
                                     "OK", () => { CGUIPopUp.instance.Hide(); });
#elif UNITY_ANDROID
            CGUIPopUp.instance.ShowQuestion(LocalizationManager.GetTermTranslation("Info"),
                                    LocalizationManager.GetTermTranslation("showRewardedAdFreeEnergy"),
                                    LocalizationManager.GetTermTranslation("YES"), () => { CGUIPopUp.instance.Hide(); AdvertisementService.instance.ShowRewardedAds(rewardedAdsTypes.freeEnergy); },
                                    LocalizationManager.GetTermTranslation("NO"), () => { CGUIPopUp.instance.Hide(); });
#elif UNITY_IOS
            CGUIPopUp.instance.ShowQuestion(LocalizationManager.GetTermTranslation("Info"),
                                    LocalizationManager.GetTermTranslation("showRewardedAdFreeEnergy"),
                                    LocalizationManager.GetTermTranslation("YES"), () => { CGUIPopUp.instance.Hide(); AdvertisementService.instance.ShowRewardedAds(rewardedAdsTypes.freeEnergy); },
                                    LocalizationManager.GetTermTranslation("NO"), () => { CGUIPopUp.instance.Hide(); });
#else
            CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                                     LocalizationManager.GetTermTranslation("unexpectedPlatform"),
                                     "OK", () => { CGUIPopUp.instance.Hide(); });
#endif
        }
        else
        {
            TimeSpan ts = TimeSpan.FromSeconds(Player.instance.GetEnergyTimeRemaining());

            CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                                     LocalizationManager.GetTermTranslation("RemainingTime") + " - " +
                                     ts.Days + ":" + ts.Hours + ":" + ts.Minutes + ":" + ts.Seconds,
                                     "OK", () => { CGUIPopUp.instance.Hide(); });
        }
    }

    public void AnimateMoney()
    {
        StartCoroutine(AnimateMoneyEnumarator());
    }

    IEnumerator AnimateMoneyEnumarator()
    {
        moneyAnimator.gameObject.SetActive(true);
        yield return new WaitForSeconds(1f);
        moneyAnimator.gameObject.SetActive(false);

    }
    public void AnimateCrimePoints()
    {
        StartCoroutine(AnimateCrimePointsEnumarator());
    }

    IEnumerator AnimateCrimePointsEnumarator()
    {
        crimePointsAnimator.gameObject.SetActive(true);
        yield return new WaitForSeconds(1f);
        crimePointsAnimator.gameObject.SetActive(false);

    }

    public void AnimateSTR()
    {
        StartCoroutine(AnimateSTREnumarator());
    }

    IEnumerator AnimateSTREnumarator()
    {
        strAnimator.gameObject.SetActive(true);
        yield return new WaitForSeconds(1f);
        strAnimator.gameObject.SetActive(false);

    }

    public void AnimateINT()
    {
        StartCoroutine(AnimateINTEnumarator());
    }

    IEnumerator AnimateINTEnumarator()
    {
        intAnimator.gameObject.SetActive(true);
        yield return new WaitForSeconds(1f);
        intAnimator.gameObject.SetActive(false);

    }
}
