﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public partial class CGUIPopUp : MonoBehaviour {

    public static CGUIPopUp instance;

    public GameObject panel;
    public Text Header;
    public Text Description;
    public Button YesButton;
    public Button NoButton;
    public Text YesButtonText;
    public Text NoButtonText;

    private void Awake()
    {
        instance = this;
    }

    public void ShowInfo(string header, string description, string yesButtonText, UnityAction yesButtonAction)
    {
        Header.text = header;
        Description.text = description;
        YesButtonText.text = yesButtonText;

        Header.gameObject.SetActive(true);
        Description.gameObject.SetActive(true);
        YesButton.gameObject.SetActive(true);
        NoButton.gameObject.SetActive(false);

        YesButton.onClick.RemoveAllListeners();
        YesButton.onClick.AddListener((yesButtonAction));

        panel.SetActive(true);
    }

    public void ShowQuestion(string header, string description, string yesButtonText, UnityAction yesButtonAction, string noButtonText, UnityAction noButtonAction)
    {
        Header.text = header;
        Description.text = description;
        YesButtonText.text = yesButtonText;
        NoButtonText.text = noButtonText;

        Header.gameObject.SetActive(true);
        Description.gameObject.SetActive(true);
        YesButton.gameObject.SetActive(true);
        NoButton.gameObject.SetActive(true);

        YesButton.onClick.RemoveAllListeners();
        YesButton.onClick.AddListener((yesButtonAction));

        NoButton.onClick.RemoveAllListeners();
        NoButton.onClick.AddListener((noButtonAction));

        panel.SetActive(true);
    }

    public void ShowStatus(string header, string description)
    {
        Header.text = header;
        Description.text = description;

        Header.gameObject.SetActive(true);
        Description.gameObject.SetActive(true);
        YesButton.gameObject.SetActive(false);
        NoButton.gameObject.SetActive(false);

        panel.SetActive(true);
    }

    public void Hide()
    {
        panel.SetActive(false);
    }
}

public enum CGUIPopUpTypes
{
    Info = 0,
    Question = 1,
    Status = 2,
}

