﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class CGUIPopUpExample : MonoBehaviour {

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
        
		if(Input.GetKeyDown(KeyCode.Q))
        {
            //Question Exaple
            CGUIPopUp.instance.ShowQuestion("1 Question", "Do You Like This Addon", "YES", () => CGUIPopUp.instance.Hide(), "NO", () => CGUIPopUp.instance.Hide());
        }

        if (Input.GetKeyDown(KeyCode.S))
        {
            //Status Example
            CGUIPopUp.instance.ShowStatus("Connecting", "Connecting To Server");
        }

        if (Input.GetKeyDown(KeyCode.I))
        {
            //Info Example
            CGUIPopUp.instance.ShowInfo("Logged - In", "Logged Successfully", "OK", () => CGUIPopUp.instance.Hide());
        }

        if (Input.GetKeyDown(KeyCode.H))
        {
            //Hide Example
            CGUIPopUp.instance.Hide();
        }
    }

}
